<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\User\UserController;

Route::group(
	[
		'prefix'     => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect'],
	], function () {
	Route::group(['prefix' => \Config::get('app.admin-url'), 'namespace' => 'Admin'], function () {
		Route::get('/login', [LoginController::class, 'showLoginForm'])->name('admin.login');
		Route::post('/login', [LoginController::class, 'login']);
		Route::post('/logout', [LoginController::class, 'logout'])->name('admin.logout');
	});
	Route::group(['prefix' => \Config::get('app.admin-url'), 'namespace' => 'Admin', 'middleware' => ['admin.auth', 'auth:admin', 'bindings']], function () {
		Route::get('/', 'IndexController@index')->name('admin.index');
		/** @see UserController */
		Route::resource('users', 'User\UserController', ['as' => 'admin', 'except' => ['show']]);
		Route::match(['GET', 'POST'], '/users/signsuperadmin', [UserController::class, 'signSuperAdmin'])->name('signsuperadmin');
		Route::group(['prefix' => 'profile', 'namespace' => 'User'], function () {
			Route::get('/', 'UserController@profile')->name('admin.profile');
			Route::post('/', 'UserController@profileUpdate')->name('admin.profile.update');
		});

		Route::resources([
			'translate' => 'TranslateController',
			'settings'  => 'SettingController',
		]);

		Route::resources([
			'reviews'     => 'ReviewController',
			'services'    => 'ServiceController',
			'rooms'       => 'RoomController',
			'roles'       => 'RoleController',
			'menu'        => 'MenuController',
			'admin-menus' => 'AdminMenuController',
		], ['as' => 'admin', 'except' => ['show']]
		);


		Route::get('/test/{menu}', [\App\Http\Controllers\Admin\MenuController::class, 'edit']);

		Route::post('/admin-menus/nesting', 'AdminMenuController@nesting')->name('admin.admin-menus.nesting');
		Route::patch('/admin-menus/save/all', 'AdminMenuController@updateAll')->name('admin-menus.updateAll');

		Route::resource('menu-group', 'MenuGroupController', ['parameters' => ['menu-group' => 'menuGroup']]);

		Route::group(['namespace' => 'Meta'], function () {
			/** @see \App\Http\Controllers\Admin\Meta\MetaController */
			Route::resource('meta', 'MetaController', ['parameters' => ['meta' => 'meta'], 'as' => 'admin', 'except' => ['show']]);
			Route::resource('redirects', 'RedirectController', ['as' => 'admin', 'except' => ['show']]);

			/** @see \App\Http\Controllers\Admin\Meta\RobotController::index() */
			Route::get('/robots', 'RobotController@index')->name('admin.robots.index');
			Route::put('/robots', 'RobotController@update')->name('admin.robots.update');

			Route::resource('/sitemap', 'SitemapController', ['only' => ['index', 'store'], 'as' => 'admin']);
		});

		Route::group(['prefix' => 'photos'], function () {
			Route::post('/edit', ['uses' => 'PhotosController@edit']);
			Route::post('/delete', ['uses' => 'PhotosController@delete']);
			Route::match(['get', 'post'], '/get-cropper', ['uses' => 'PhotosController@getPhotoCropper']);
		});
		Route::group(['as' => 'settings.', 'prefix' => 'settings',], function () {
			Route::get('{id}/delete_value', [
				'uses' => 'SettingController@delete_value',
				'as'   => 'delete_value',
			]);
		});

		Route::post('/menu/nesting', 'MenuController@nesting')->name('admin.menu.nesting');
		Route::group(['prefix' => 'ajax'], function () {
			Route::post('/sort', 'AjaxController@sort')->name('sort');
			Route::post('/delete', 'AjaxController@delete')->name('delete');
		});


		Route::prefix('dashboard')
			->group(function () {
				Route::group(['prefix' => 'cache'], function () {
					/** @see IndexController::clearCache() */
					Route::post('/clear', 'IndexController@clearCache')->name('cache.clear');
					/** @see IndexController::clearView() */
					Route::post('/clear/view', 'IndexController@clearView')->name('cache.view');
				});
				Route::post('/artisan/storage-link', 'IndexController@storageLink')->name('artisan.storage.link');
				Route::post('/artisan/refresh-db', 'IndexController@refreshDb')->name('artisan.db.refresh');
			})
		;

		Route::get('logs', 'Staff\\LogViewController@index');

	});

});