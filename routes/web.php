<?php

	use App\Http\Controllers\HomeController;
	use App\Http\Controllers\PageController;
	use App\Http\Controllers\ServiceController;

	Route::group(
		[
			'prefix'     => LaravelLocalization::setLocale(),
			'middleware' => ['localizationRedirect'],
		], function () {

		Route::get('/', [HomeController::class, 'index'])->name('home');
		Route::get('/contacts', [PageController::class, 'contacts'])->name('contacts');
		Route::get('/about', [PageController::class, 'about'])->name('about');
		Route::get('/services', [ServiceController::class, 'index'])->name('services');
		Route::get('/reservation', [\App\Http\Controllers\ReservationController::class, 'index'])->name('reservation');
		Route::post('/reviews', [\App\Http\Controllers\ReviewController::class, 'store'])->name('reviews.store');

	});