$(document).ready(function (e) {
   $('.rooms__slider_items').slick({
      centerMode: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
         {
            breakpoint: 900,
            settings: {
               slidesToShow: 2,
               slidesToScroll: 1,
               infinite: true,
            }
         },
         {
            breakpoint: 635,
            settings: {
               slidesToShow: 1,
               slidesToScroll: 1,
               infinite: true,
            }
         }
      ]
   });

   $('.rooms__slider_img').slick({
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      swipe: false,
      prevArrow: '<button id="prev" type="button" class="slick-arrows btn-prev"><img src="./src/images/icons/rooms/slickArrow.svg" alt="prevArrow"></button>',
      nextArrow: '<button id="next" type="button" class="slick-arrows btn-next"><img src="./src/images/icons/rooms/slickArrow.svg" alt="nextArrow"></button>',

   });

   $('.reviews__slider').slick({
      centerMode: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
         {
            breakpoint: 900,
            settings: {
               slidesToShow: 2,
               slidesToScroll: 1,
               infinite: true,
            }
         },
         {
            breakpoint: 700,
            settings: {
               slidesToShow: 1,
               slidesToScroll: 1,
               infinite: true,
            }
         }
      ]
   });

   $('.services__slick').slick({
      centerMode: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
   })

})