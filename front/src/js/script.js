new WOW().init();

//MODAL WINDOW
$('#open').click(function () {
   $('body').addClass('lock');
   $('.modal__container').css({'transform': 'translateY(0px)'});
});

$('#close').click(function () {
   $('body').removeClass('lock');
   $('.modal__container').css({'transform': 'translateY(-200%)'});
})

//MORE BTN
function BtnMore() {
   $('.moreBtn').click(function () {
      const btn = $(this);
      const parent = btn.parent('.reviews__slider_item');
      const text = parent.find('.reviews__slider_description');
      text.toggleClass('active')
      btn.toggleClass('less')
   })
}

BtnMore();

//DATEPICKER
$(function () {
   $("#arrival").datepicker({
      minDate: 0,
      dateFormat: "dd.mm.yy",
      dayNamesMin: ["пн", "вт", "ср", "чт", "пт", "сб", "вс"],
      monthNames: ["Январь", "Февраль ", "Март", "Апрель ", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
   }).datepicker("setDate", new Date());

   $("#departure").datepicker({
      minDate: 0,
      dateFormat: "dd.mm.yy",
      dayNamesMin: ["пн", "вт", "ср", "чт", "пт", "сб", "вс"],
      monthNames: ["Январь", "Февраль ", "Март", "Апрель ", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
   }).datepicker("setDate", new Date());


   $('#arrival').change( function () {
      let date = $(this).val();
      $('#departure').val(date)
   });

   $("#arrivalPopup").datepicker({
      minDate: 0,
      dateFormat: "dd.mm.yy",
      dayNamesMin: ["пн", "вт", "ср", "чт", "пт", "сб", "вс"],
      monthNames: ["Январь", "Февраль ", "Март", "Апрель ", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
   }).datepicker("setDate", new Date());

   $("#departurePopup").datepicker({
      minDate: 0,
      dateFormat: "dd.mm.yy",
      dayNamesMin: ["пн", "вт", "ср", "чт", "пт", "сб", "вс"],
      monthNames: ["Январь", "Февраль ", "Март", "Апрель ", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
   }).datepicker("setDate", new Date());


   $('#arrivalPopup').change( function () {
      let date = $(this).val();
      $('#departure').val(date)
   });


});

//POPUP
$(function () {
   let closePopup = (e) => {
      if(e.target.className === 'popup' || e.target.className === 'popup__block_close close-popup') {
         $('.popup').fadeOut(600);
         $('html').removeClass('lockScroll');
      }
   }

   let closePopupForm = (e) => {
      if(e.target.className === 'popup' || e.target.className === 'popup__block_close close-popup') {
         $('#formPopup').fadeOut(600);
         $('html').removeClass('lockScroll');
      }
   }

   $('.open-popup').click(function () {
      $('#reviewsPopup').fadeIn(600);
      $('html').addClass('lockScroll');
   });
   $('#reviewsPopup').click(closePopup)

   $('.open-popupForm').click(function () {
      $('#formPopup').fadeIn(600);
      $('html').addClass('lockScroll');
   });
   $('#formPopup').click(closePopupForm)
})

//SCROLL FUNC
$(window).scroll(function () {

   const $img = $('#fixedImg');
   const $phone = $('#fixedPhone');

   if($(this).scrollTop() > 100 && $(this).width() <= 635) {
      $('#fixedHeader').addClass('fixed');
      $('#open>.fixed').css('background-color', '#0000');
      $('#fixedMulti').css('color', '#0000');
      $img.attr('src', './src/images/icons/logoDark.svg');
      $phone.attr('src', './src/images/icons/IconTelDark.svg');
   } else {
      $('#fixedHeader').removeClass('fixed');
      $('#open>.fixed').css('background-color', '#fffffff');
      $('#fixedMulti').css('color', '#fffffff');
      $img.attr('src', './src/images/icons/logo.svg');
      $phone.attr('src', './src/images/icons/IconTel.svg');
   }
})



