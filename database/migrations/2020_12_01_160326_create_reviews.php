<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviews extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'reviews';

   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }

   public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

			$table->id();

            $this->builder
	            ->createName()
				->createNullableString('review', 5000)
                ->createActive()
				->createNullableChar('ip')
            ;
            $table->timestamps();
        });



    }


    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
