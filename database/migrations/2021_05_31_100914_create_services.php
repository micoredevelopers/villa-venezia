<?php

	use App\Builders\Migration\MigrationBuilder;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateServices extends Migration
	{

		/**
		 * @var MigrationBuilder
		 */
		private $builder;

		private $table = 'services';

		private $foreignKey = 'service_id';

		private $tableLang = 'service_lang';

		public function __construct()
		{
			$this->builder = app(MigrationBuilder::class);
		}

		public function up()
		{
			Schema::create($this->table, function (Blueprint $table) {
				$this->builder->setTable($table);

				$table->id();
				$this->builder
					->createImage()
					->createImage('icon')
					->createSort()
					->createActive()
					->createBoolean('free')
				;
				$table->timestamps();
			});


			Schema::create($this->tableLang, function (Blueprint $table) {
				$this->builder->setTable($table);
				$table->id();
				$table->unsignedBigInteger($this->foreignKey);

				$this->builder
					->createName()
					->createExcerpt()
					->createDescription()
					->createLanguageKey()
				;
				$table->foreign($this->foreignKey)
					->references('id')->on($this->table)
					->onUpdate('cascade')->onDelete('cascade')
				;
			});
		}

		public function down()
		{
			Schema::dropIfExists($this->tableLang);
			Schema::dropIfExists($this->table);
		}
	}
