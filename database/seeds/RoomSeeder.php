<?php

	namespace Database\Seeders;

	use App\Models\Room\Room;
	use App\Repositories\RoomRepository;

	class RoomSeeder extends AbstractSeeder
	{

		/**
		 * @var \Faker\Generator
		 */
		protected $factory;

		private $repository;

		public function __construct(\Faker\Generator $factory, RoomRepository $repository)
		{
			$this->factory = $factory;
			$this->repository = $repository;
			$repository->model()::reguard();
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$rooms = [
				['name' => 'Люкс 2-х комнатный', 'price' => 3270, 'excerpt' => 'Большая двуспальная кровать - Диван-кровать - Кондиционер-Гидромассажная ванна - Биде - Отдельная гостинная с обеденным столом - Рабочее место - Балкон с видом на море - Завтрак включен'],
				['name' => 'Люкс', 'price' => 2850, 'excerpt' => 'Двуспальная кровать - Кондиционер - Джакузи или душевая кабинка - Балкон с видом на море - Завтрак включен'],
				['name' => 'Полулюкс', 'price' => 2280, 'excerpt' => 'Двуспальная кровать - Кондиционер - Джакузи или душевая кабинка - Завтрак включен'],
				['name' => 'Улучшенный стандарт', 'price' => 1590, 'excerpt' => 'Двуспальная кровать - Кондиционер - Завтрак включен'],
				['name' => 'Стандарт', 'price' => 1265, 'excerpt' => 'Двуспальная кровать - Кондиционер - Завтрак включен'],
			];
			foreach ($rooms as $i => $room) {
				$room['image'] = $this->randomImage();
				$room['sort'] = $i;
				/** @var  $room Room*/
				$room = $this->repository->create($room);
				foreach ([1,2,3] as $s){
					$room->images()->create(['image' => $this->image()]);
				}
			}
		}

		private function randomImage()
		{
			$path = '/images/icons/rooms/room%d.jpg';
			$path = sprintf($path, random_int(1,3));
			return asset($path);
		}
	}
