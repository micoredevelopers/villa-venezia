<?php
	namespace Database\Seeders;

	use App\Models\Admin\AdminMenu;
	use App\Repositories\Admin\AdminMenuRepository;
	use Illuminate\Database\Seeder;
	use Illuminate\Support\Arr;
	use Illuminate\Support\Facades\Artisan;

	class AdminMenuSeeder extends Seeder
	{
		private $adminMenuRepository;

		private $menusByUrl;

		public function __construct(AdminMenuRepository $adminMenuRepository)
		{
			$this->adminMenuRepository = $adminMenuRepository;
			$this->menusByUrl = $this->adminMenuRepository->all()->keyBy('url');
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$menus = [
				[
					'name'      => 'Настройки',
					'url'       => '/settings',
					'gate_rule' => 'view_settings',
					'icon_font' => '<i class="material-icons">settings</i>',
				],
				[
					'name'      => 'Локализация',
					'url'       => '/translate',
					'gate_rule' => 'view_translate',
					'icon_font' => '<i class="fa fa-language" aria-hidden="true"></i>',
				], [
					'active'    => 0,
					'name'      => 'SEO',
					'url'       => '/meta',
					'gate_rule' => 'view_meta',
					'icon_font' => '<i class="fa fa-google-plus" aria-hidden="true"></i>',
					'childrens' => [
						[
							'name'      => 'Redirects',
							'url'       => '/redirects',
							'gate_rule' => 'view_redirects',
							'icon_font' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
						],
						[
							'name'      => 'Sitemap',
							'url'       => '/sitemap',
//						'gate_rule' => 'view_sitemap',
							'icon_font' => '<i class="fa fa-sitemap" aria-hidden="true"></i>',
						],
						[
							'name'      => 'Robots.txt',
							'url'       => '/robots',
							'gate_rule' => 'view_robots',
							'icon_font' => '<i class="fa fa-android" aria-hidden="true"></i>',
						],
					],
				],
				[
					'name'      => 'Пользователи',
					'url'       => '/users',
					'gate_rule' => 'view_users',
					'icon_font' => '<i class="fa fa-user" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Услуги',
					'url'       => '/services',
					'gate_rule' => 'view_services',
					'icon_font' => '',
				],
				[
					'name'      => 'Номера',
					'url'       => '/rooms',
					'gate_rule' => 'view_rooms',
					'icon_font' => '',
				],
				[
					'name'      => 'Отзывы',
					'url'       => '/reviews',
					'gate_rule' => 'view_reviews',
					'icon_font' => '',
				],
				[
					'active'    => 0,
					'name'      => 'Роли',
					'url'       => '/roles',
					'gate_rule' => 'view_roles',
					'icon_font' => '<i class="fa fa-users" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Admin Menu',
					'url'       => '/admin-menus',
					'gate_rule' => 'view_admin-menus',
					'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Меню',
					'url'       => '/menu?group=1',
					'gate_rule' => 'view_menu',
					'icon_font' => '<i class="fa fa-bars" aria-hidden="true"></i>',
				],
				[
					'name'      => 'Logs',
					'url'       => '/logs',
					'sort'      => '20',
					'gate_rule' => 'view_logs',
					'icon_font' => '<i class="fa fa-history" aria-hidden="true"></i>',
				],
			];
			$this->loop($menus);

			Artisan::call('cache:clear');
		}

		private function loop(array $menus, ?AdminMenu $parentMenu = null)
		{
			foreach ($menus as $menu) {
				$menuModel = $this->createMenu($menu, $parentMenu);
				if ($menuModel && Arr::get($menu, 'childrens')) {
					$this->loop(\Arr::wrap(Arr::get($menu, 'childrens')), $menuModel);
				}
			}
		}

		private function createMenu(array $menu, ?AdminMenu $parentMenu = null): ?AdminMenu
		{
			if ($this->isMenuExistsByUrl($menu['url'] ?? '')) {
				return $this->getMenuExistsByUrl($menu['url'] ?? '');
			}
			$menu = array_merge(['active' => 1], $menu);
			if ($parentMenu) {
				$menu['parent_id'] = $parentMenu->getKey();
			}

			try{
				return tap($this->adminMenuRepository->create($menu), function ($adminMenu) {
					if ($adminMenu) {
						$this->onCreated($adminMenu);
					}
				});
			} catch (\Throwable $e){
			    d(get_defined_vars());
			}
		}

		private function onCreated(AdminMenu $adminMenu)
		{

		}

		private function isMenuExistsByUrl(?string $url)
		{
			return $this->menusByUrl->offsetExists($url);
		}

		private function getMenuExistsByUrl(?string $url)
		{
			return $this->menusByUrl->get($url);
		}

		private function getBuilder()
		{
			return new class implements \ArrayAccess {
				private $active = true;

				private $url = '';

				private $gateRule = '';

				private $name = '';

				private $iconFont = '';

				private $childrens = [];

				public function setName(string $_): self
				{
					$this->name = $_;
					return $this;
				}

				/**
				 * @param string $url
				 */
				public function setUrl(string $url): self
				{
					$this->url = $url;
					return $this;
				}

				/**
				 * @param string $_
				 */
				public function setGateRule(string $_): self
				{
					$this->gateRule = $_;
					return $this;
				}

				/**
				 * @param string $_
				 */
				public function setIconFont(string $_): self
				{
					$this->iconFont = $_;
					return $this;
				}

				/**
				 * @param bool $active
				 */
				public function setActive(bool $active): self
				{
					$this->active = $active;
					return $this;
				}

				/**
				 * @param array $_
				 */
				public function setChildrens(array $_): self
				{
					$this->childrens = $_;
					return $this;
				}

				public function build()
				{
					$menu = [
						'active'    => (int)$this->active,
						'url'       => $this->url,
						'gate_rule' => $this->gateRule,
						'name'      => $this->name,
						'icon_font' => $this->iconFont,
					];
					if ($this->childrens) {
						$menu['childrens'] = $this->childrens;
					}
					return $menu;
				}

				public function offsetExists($offset)
				{
					return property_exists($this, $offset);
				}

				public function offsetGet($offset)
				{
					return $this->offsetExists($offset) ? $this->{$offset} : null;
				}

				public function offsetSet($offset, $value)
				{
					$method = Str::camel('set' . ucfirst($offset));
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}

				public function offsetUnset($offset)
				{
					if ($this->offsetExists($offset)) {
						$this->{$offset} = null;
					}
				}

			};
		}
	}
