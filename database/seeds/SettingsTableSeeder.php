<?php
	namespace Database\Seeders;
	use App\Models\Setting;
	use App\Repositories\Admin\SettingsRepository;
	use Illuminate\Database\Seeder;

	class SettingsTableSeeder extends Seeder
	{
		private $settings;
		/**
		 * @var SettingsRepository
		 */
		private $repository;

		public function __construct(SettingsRepository $repository)
		{
			$this->settings = $repository->withTrashed()->get()->keyBy('key');
			$this->repository = $repository;
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$groups = [
				'global'    => [
					$this->getBuilder('sitename')->setValue('Villa-venezia')->setDisplayName('Название сайта')->build(),
					$this->getBuilder('phone-one')->setValue('+38(0482)34-26-36')->setDisplayName('Номер телефона')->build(),
					$this->getBuilder('phone-two')->setValue('+38(0482)34-69-34')->setDisplayName('Номер телефона')->build(),
					$this->getBuilder('address')->setValue('https://www.google.com.ua/maps/place/%D1%83%D0%BB.+%D0%9A%D0%B0%D0%BC%D0%B0%D0%BD%D0%B8%D0%BD%D0%B0,+16,+%D0%9E%D0%B4%D0%B5%D1%81%D1%81%D0%B0,+%D0%9E%D0%B4%D0%B5%D1%81%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+65000/@46.4244934,30.7592987,16.96z/data=!4m5!3m4!1s0x40c6347833445ad3:0xac1130b8391b8dd6!8m2!3d46.4245232!4d30.7614454?hl=ru')->setDisplayName('Ссылка на google maps с адресом')->build(),
					$this->getBuilder('public-email')->setValue('venice.hot@gmail.com')->setDisplayName('Публичный E-mail для контактов с пользователями')->build(),
				],
				'google'    => [
					$this->getBuilder('gtag-head')->setDisplayName('Google tag Head')->setTypeTextarea()->setValue('')->build(),
					$this->getBuilder('gtag-body')->setDisplayName('Google tag Body')->setTypeTextarea()->setValue('')->build(),
				],
				'socials' => [
					$this->getBuilder('facebook')->setDisplayName('Facebook')->setValue('https://www.facebook.com/')->build(),
					$this->getBuilder('instagram')->setDisplayName('Instagram')->setValue('https://instagram.com/villavenezia_villaneapol_hotel')->build(),
				],
			];
			$groups = array_reverse($groups);
			foreach ($groups as $groupName => $settings) {
				foreach ($settings as $setting) {
					$setting['group'] = $groupName;
					$setting['key'] = implode('.', [$groupName, $setting['key']]);
					if ($this->settingExists($setting['key'])) {
						continue;
					}
					$this->repository->create($setting);
				}
			}
		}

		private function getSettingByKey(string $key): ?Setting
		{
			return \Arr::get($this->settings, $key);
		}

		private function settingExists(string $key): bool
		{
			return (bool)$this->getSettingByKey($key);
		}

		private function getBigText(string $id)
		{
			$arr = [
				'seo.head-global-codes' => '',
			];

			return \Arr::get($arr, $id);
		}

		private function getBuilder(?string $key)
		{
			$builder = new class implements \ArrayAccess {
				private $key = '';

				private $value = '';

				private $type = 'text';

				private $displayName = '';

				public function setKey(string $_): self
				{
					$this->key = $_;
					return $this;
				}

				public function setType(string $_): self
				{
					$this->type = $_;
					return $this;
				}

				public function setTypeTextarea(): self
				{
					$this->type = 'rich_text_box';
					return $this;
				}

				public function setTypeEditor(): self
				{
					$this->type = 'ckeditor';
					return $this;
				}

				public function setTypeFile(): self
				{
					$this->type = 'file';
					return $this;
				}

				public function setTypeCheckbox(): self
				{
					$this->setType('checkbox');
					return $this;
				}

				public function setValue(?string $_ = null): self
				{
					$this->value = $_;
					return $this;
				}

				public function setDisplayName(string $_): self
				{
					$this->displayName = $_;
					return $this;
				}

				public function build()
				{
					return [
						'key'          => $this->key,
						'value'        => $this->value,
						'type'         => $this->type,
						'display_name' => $this->displayName,
					];
				}

				public function offsetExists($offset)
				{
					return property_exists($this, $offset);
				}

				public function offsetGet($offset)
				{
					return $this->offsetExists($offset) ? $this->{$offset} : null;
				}

				public function offsetSet($offset, $value)
				{
					$method = Str::camel('set' . ucfirst($offset));
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}

				public function offsetUnset($offset)
				{
					if ($this->offsetExists($offset)) {
						$this->{$offset} = null;
					}
				}

			};
			if ($key) {
				$builder->setKey($key);
			}
			return $builder;
		}

	}
