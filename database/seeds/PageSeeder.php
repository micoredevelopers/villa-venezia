<?php
	namespace Database\Seeders;
	use \Illuminate\Support\Collection;
	use App\Models\Page\Page;
	use App\Models\Page\PageLang;
	use App\Repositories\PageRepository;

	class PageSeeder extends AbstractLanguageableSeeder
	{
		private $existsPages;
		/**
		 * @var PageRepository
		 */
		private $repository;

		public function __construct(PageRepository $repository)
		{
			return;
			parent::__construct();
			$this->setExistsPages($repository->all());
			$this->repository = $repository;
		}

		/**
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		public function run()
		{
			return;
			$pages = [
				[
					'title'           => 'О клинике',
					'name'            => 'О нас',
					'url'             => 'about-us',
					'page_type'       => 'about',
					'manual'          => false,
				],
			];
			$this->loop($pages);
		}

		/**
		 * @param array     $pages
		 * @param Page|null $parentPage
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		private function loop(array $pages, Page $parentPage = null)
		{
			foreach ($pages as $page) {
				if ($this->pageExistsByUrl($page['url'] ?? '')) {
					continue;
				}
				$pageModel = $this->createPage($page, $parentPage);
				if (Arr::has($page, 'pages')) {
					$this->loop(Arr::get($page, 'pages'), $pageModel);
				}
			}
		}

		/**
		 * @param array     $pageData
		 * @param Page|null $parentPage
		 * @return Page|mixed
		 * @throws \Illuminate\Contracts\Container\BindingResolutionException
		 */
		private function createPage(array $pageData, Page $parentPage = null): Page
		{
			$pageData = array_merge(['manual' => 1], $pageData);
			if ($parentPage) {
				$pageData['parent_id'] = $parentPage->getKey();
			}
			$pageModel = $this->repository->create($pageData);

			return $pageModel;
		}

		private function setExistsPages(Collection $collection)
		{
			$this->existsPages = $collection->keyBy('url');
		}

		private function pageExistsByUrl(string $url)
		{
			return $this->existsPages->offsetExists($url);
		}
	}
