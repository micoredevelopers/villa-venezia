<?php

namespace Database\Seeders;

use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TranslateTableSeeder extends Seeder
{
	private $translates;
	/**
	 * @var TranslateRepository
	 */
	private $repository;

	public function __construct(TranslateRepository $repository)
	{
		$this->translates = $repository->all()->keyBy('key');
		$this->repository = $repository;
	}

	public function run()
	{
		Translate::reguard();
		$groups = [
			'global' => [
				['key' => 'yes', 'value' => 'Да'],
				['key' => 'currency', 'value' => 'Грн.'],
				['key' => 'no', 'value' => 'Нет'],
				['key' => '404', 'value' => 'Страница не найдена'],
				['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адресс', 'type' => 'textarea'],
			],

			'contacts' => [
				$this->getBuilder('title')->setValue('Контакты')->build(),
				$this->getBuilder('top-description')->setTypeTextarea()->setValue('Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. В числе удобств бесплатный Wi-Fi и бесплатная охраняемая парковка. В отеле работают крытый и открытый бассейны, а также имеется гидромассажная ванна под открытым небом.  Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. В числе удобств бесплатный Wi-Fi и бесплатная охраняемая парковка. В отеле работают крытый и открытый бассейны, а также имеется гидромассажная ванна под открытым небом. ')->build(),
			],
			'about' => [
				$this->getBuilder('title')->setValue('О нас')->build(),
			],
			'reservation' => [
				$this->getBuilder('title')->setValue('Бронирование')->build(),
			],
			'reviews' => [
				$this->getBuilder('add.success')->setValue('Ваш отзыв успешно добавлен')->build(),
				$this->getBuilder('add.fail')->setValue('Произошла ошибка, ваш отзыв не был добавлен, пожалуйста попробуйте позже')->build(),
				$this->getBuilder('show-full')->setValue('Показать польностью')->build(),
				$this->getBuilder('show-less')->setValue('Свернуть')->build(),
				$this->getBuilder('modal-description')->setValue('Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. ')->build(),
			],
			'services' => [
				$this->getBuilder('title')->setValue('Услуги')->build(),
				$this->getBuilder('free')->setValue('Бесплатно')->build(),
			],
			'form' => [
				$this->getBuilder('review-placeholder')->setValue('Расскажите о нас')->build(),
				$this->getBuilder('review')->setValue('Отзыв')->build(),
				$this->getBuilder('send')->setValue('ОТПРАВИТЬ')->build(),
				$this->getBuilder('full_name')->setValue('Фамилия, имя')->build(),
				$this->getBuilder('full_name-placeholder')->setValue('Андреев Андрей')->build(),
				$this->getBuilder('in')->setValue('Заезд')->build(),
				$this->getBuilder('out')->setValue('Выезд')->build(),
				$this->getBuilder('guests')->setValue('Количество гостей')->build(),
				$this->getBuilder('search')->setValue('Искать')->build(),
				$this->getBuilder('middle-title')->setValue('Мини-отель в живописном уголке Одессы')->build(),
				$this->getBuilder('middle-description')->setTypeTextarea()->setValue('The ultimate sophisticate and arbiter, The St. Regis Venice unfolds a new era of glamour at the city’s finest address on the Grand Canal. ')->build(),
			],
			'footer' => [
				$this->getBuilder('title')->setValue('Уютный мини-отель ')->build(),
				$this->getBuilder('description')->setValue('Отель расположен в курортной зоне Одессы — Аркадии. Улица Дерибасовская, Потёмкинская лестница, а также Музей Пушкина находятся в 6 км. ')->build(),
				$this->getBuilder('contacts')->setValue('Контакты')->build(),
				$this->getBuilder('contacts.address')->setValue('Ул. Каманина,16')->build(),
				$this->getBuilder('reservate')->setValue('Забронировать')->build(),
			],
			'main'       => [
				$this->getBuilder('title')->setValue('Villa Venezia')->build(),
				$this->getBuilder('top.title')->setValue('Мини-отель в живописном уголке Одессы ')->build(),
				$this->getBuilder('top-description')->setValue('The ultimate sophisticate and arbiter, The St. Regis Venice unfolds a new era of glamour at the city’s finest address on the Grand Canal. ')->build(),
				$this->getBuilder('about.title')->setValue('Почувствуйте тепло Одесского солнца в Вилле')->build(),
				$this->getBuilder('about.description')->setValue('Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. В числе удобств бесплатный Wi-Fi и бесплатная охраняемая парковка. В отеле работают крытый и открытый бассейны, а также имеется гидромассажная ванна под открытым небом. ')->build(),
				$this->getBuilder('services.title')->setValue('Сервис и развлечения')->build(),
				$this->getBuilder('rooms.title')->setValue('Номера')->build(),
				$this->getBuilder('rooms.all')->setValue('Смотреть все')->build(),
				//
				$this->getBuilder('infrastructure.title')->setValue('Инфраструктура')->build(),
				$this->getBuilder('infrastructure.description')->setValue('Отель “Вилла Венеция” находится в известнейшем курортном районе — Аркадия, всего в пяти минутах ходьбы от пляжа. В непосредственной близости лучшие рестораны и ночные клубы города. ')->build(),
				$this->getBuilder('infrastructure.one.num')->setValue('5 минут')->build(),
				$this->getBuilder('infrastructure.one.target')->setValue('до пляжа и ночных клубов')->build(),
				$this->getBuilder('infrastructure.one.description')->setValue('Всего 5 минут пешей ходьбы к морю. Рядом любимый пляж Одессы — “Аркадия”, а также оживленные ночные клубы, в том числе знаменитые «Ибица» и «Рай». ')->build(),
				$this->getBuilder('infrastructure.two.num')->setValue('10 минут ')->build(),
				$this->getBuilder('infrastructure.two.target')->setValue('до ж/д и автовокзала')->build(),
				$this->getBuilder('infrastructure.two.description')->setValue('Железнодорожный вокзал и автовокзал расположены всего в 10-15 минутах езды на автомобиле. ')->build(),
				$this->getBuilder('infrastructure.three.num')->setValue('15 минут ')->build(),
				$this->getBuilder('infrastructure.three.target')->setValue('до центра города')->build(),
				$this->getBuilder('infrastructure.three.description')->setValue('Отель расположен в курортной зоне Одессы — Аркадии. Улица Дерибасовская, Потёмкинская лестница, а также Музей Пушкина находятся в 6 км. ')->build(),
				//
				$this->getBuilder('reviews.title')->setValue('Отзывы')->build(),
				$this->getBuilder('reviews.add')->setValue('Оставить отзыв')->build(),
			],
		];
		foreach ($groups as $groupName => $group) {
			foreach ($group as $key => $item) {
				if (!is_array($item)) {
					$item = ['key' => $key, 'value' => $item];
				}
				$item['group'] = $groupName;
				$item['key'] = $this->addPrefixGroupToKey($item['key'], $groupName);
				if ($this->translateExists($item['key'])) {
					continue;
				}
				$translate = $this->repository->create($item);
				$this->addTranslate($translate->getAttribute('key'), $translate);
			}
		}
	}

	private function getTranslateByKey(string $key): ?Translate
	{
		return \Arr::get($this->translates, $key);
	}

	private function translateExists(string $key): bool
	{
		return (bool)$this->getTranslateByKey($key);
	}

	private function addPrefixGroupToKey(string $key, string $prefix)
	{
		return implode('.', [$prefix, $key]);
	}

	private function getBuilder(?string $key = null)
	{
		$builder = new class implements \ArrayAccess {
			private $key = '';

			private $value = '';

			private $type = 'text';

			private $displayName = '';

			private $subGroup = '';

			/** @var array */
			private $variables = [];

			public function setKey(string $key): self
			{
				$this->key = $key;
				return $this;
			}

			public function setTypeText(): self
			{
				$this->type = Translate::TYPE_TEXT;
				return $this;
			}

			public function setTypeTextarea(): self
			{
				$this->type = Translate::TYPE_TEXTAREA;
				return $this;
			}

			public function setTypeEditor(): self
			{
				$this->type = Translate::TYPE_EDITOR;
				return $this;
			}

			public function setValue(?string $value = null): self
			{
				$this->value = $value;
				return $this;
			}

			public function setDisplayName(string $displayName): self
			{
				$this->displayName = $displayName;
				return $this;
			}

			/**
			 * @param array $variables
			 */
			public function setVariables(array $variables): self
			{
				$this->variables = $variables;
				return $this;
			}

			public function build()
			{
				return [
					'key'          => $this->key,
					'value'        => $this->value,
					'type'         => $this->type,
					'display_name' => $this->displayName,
//					'variables'    => $this->variables,
					'sub_group'    => $this->subGroup,
				];
			}

			public function offsetGet($offset)
			{
				return $this->offsetExists($offset) ? $this->{$offset} : null;
			}

			public function offsetExists($offset)
			{
				return property_exists($this, $offset);
			}

			public function offsetSet($offset, $value)
			{
				$method = Str::camel('set' . ucfirst($offset));
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}

			public function offsetUnset($offset)
			{
				if ($this->offsetExists($offset)) {
					$this->{$offset} = null;
				}
			}

			/**
			 * @param string $subGroup
			 */
			public function setSubGroup(string $subGroup): self
			{
				$this->subGroup = $subGroup;
				return $this;
			}

		};
		if ($key) {
			$builder->setKey($key);
		}
		return $builder;
	}

	private function addTranslate(string $key, Translate $translate)
	{
		$this->translates->offsetSet($key, $translate);
	}
}
