<?php

namespace Database\Seeders;

use App\Models\Order\Order;
use App\Platform\Contract\UserTypeContract;
use App\Repositories\ReviewRepository;
use Illuminate\Database\Seeder;

class ReviewsSeeder extends Seeder
{

	/**
	 * @var \Faker\Generator
	 */
	private $factory;
	/**
	 * @var ReviewRepository
	 */
	private $repository;

	public function __construct(\Faker\Generator $factory, ReviewRepository $repository)
	{
		$this->factory = $factory;
		$this->repository = $repository;
	}

	public function run()
	{
		foreach (range(1, 5) as $m) {
			$review = [
				'name'   => $this->factory->name(),
				'review' => $this->factory->text(500),
				'active' => true,
			];
			$this->repository->create($review);
		}

	}

}
