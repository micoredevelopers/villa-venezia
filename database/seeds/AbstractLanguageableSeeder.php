<?php

	namespace Database\Seeders;

	use App\Models\Language;
	use Illuminate\Database\Seeder;

	class AbstractLanguageableSeeder extends Seeder
	{
		/**
		 * @var Language[]|\Illuminate\Database\Eloquent\Collection
		 */
		protected $languages;

		public function __construct()
		{
			$this->languages = Language::all();
		}
	}
