<?php

	namespace Database\Seeders;

	use App\Repositories\ServiceRepository;

	class ServiceSeeder extends AbstractSeeder
	{

		/**
		 * @var \Faker\Generator
		 */
		protected $factory;
		/**
		 * @var ServiceRepository
		 */
		private $repository;

		public function __construct(\Faker\Generator $factory, ServiceRepository $repository)
		{
			$this->factory = $factory;
			$this->repository = $repository;
			$repository->model()::reguard();
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$excerpt = 'Отель “Вилла Венеция” находится в известнейшем курортном районе — Аркадия, всего в пяти минутах ';
			$imageStub = asset('images/icons/services/serv%d.svg');
			$services = [
				['name' => 'Круглосуточный консьерж',],
				['name' => 'Камера хранения багажа',],
				['name' => 'Услуги прачечной',],
				['name' => 'Трансфер по городу и области',],
				['name' => 'Открытый и крытый бассейн',],
				['name' => 'Предоставление экскурсий',],
				['name' => 'Финская Сауна',],
				['name' => 'Услуга массажиста',],
			];
			foreach ($services as $i => $service) {
				$service['icon'] = sprintf($imageStub, ++$i);
				$service['excerpt'] = $excerpt;
				$service['image'] = $this->image();
				$service['sort'] = $i;
				$service['description'] = $this->factory->text();
				$this->repository->create($service);
			}
		}
	}
