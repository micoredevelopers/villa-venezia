<?php

namespace App\Contracts;


interface HasLocalized
{
	public function lang($language_id = null);

}