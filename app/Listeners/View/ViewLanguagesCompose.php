<?php

namespace App\Listeners\View;

use App\Containers\View\ViewLanguage;
use App\Helpers\LanguageKeyHelper;
use Illuminate\Support\Str;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ViewLanguagesCompose
{
	public function handle()
	{
		$languages = array_keys(LaravelLocalization::getSupportedLocales());
		$fn = static function ($langKey) {
			return new ViewLanguage(
				Str::upper($langKey),
				LaravelLocalization::getLocalizedURL($langKey),
				$langKey,
				LanguageKeyHelper::getIso($langKey),
				$langKey === getCurrentLocale()
			);
		};
		$languages = array_map($fn, $languages);
		view()->share(compact('languages'));
	}
}
