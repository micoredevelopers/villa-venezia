<?php declare(strict_types=1);

namespace App\Listeners\View;

use App\Containers\View\ReservationLanguageViewData;
use App\Helpers\LanguageKeyHelper;

class ViewBookingLanguagesCompose
{
	public function handle($event)
	{
		$viewLanguage = new ReservationLanguageViewData();
		$viewLanguage->setCurrentLanguageCode(
			LanguageKeyHelper::getIso(getCurrentLocale())
		);

		$supported = array_map(static function ($lang) {
			return LanguageKeyHelper::getIso($lang);
		}, array_keys(\LaravelLocalization::getSupportedLocales()));

		$viewLanguage->setSupportedLocales($supported);
		view()->share(compact('viewLanguage'));
	}
}
