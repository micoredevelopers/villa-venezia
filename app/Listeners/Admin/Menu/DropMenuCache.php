<?php

namespace App\Listeners\Admin\Menu;

use App\Events\Admin\MenusChanged;
use App\Models\Page;
use App\Repositories\MenuRepository;

class DropMenuCache
{
	private $menuRepository;

	/**
	 * DropMenuCache constructor.
	 * @param MenuRepository $menuRepository
	 */
	public function __construct(MenuRepository $menuRepository)
	{
		$this->menuRepository = $menuRepository;
	}

	/**
	 * @param MenusChanged $event
	 * @throws \Psr\SimpleCache\InvalidArgumentException
	 */
	public function handle(MenusChanged $event)
	{
		$cacheKey = Page::$publicMenusCacheKey;
		\Cache::forget($cacheKey);
		try{
			$this->menuRepository->getMenus();
		} catch (\Exception $exception){

		}
	}
}
