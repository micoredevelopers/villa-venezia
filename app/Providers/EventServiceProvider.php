<?php

namespace App\Providers;

use App\Events\Admin\MenusChanged;
use App\Events\Order\BeforeOrderClosedEvent;
use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderClosedEvent;
use App\Events\Order\OrderCreatedEvent;
use App\Events\Order\OrderCreateFilesUploadedEvent;
use App\Events\Order\OrderPublished;
use App\Events\Order\Reviews\AddCustomerReviewEvent;
use App\Events\Order\Reviews\AddPerformerReviewEvent;
use App\Events\Order\Reviews\BeforeAddCustomerReviewEvent;
use App\Events\Order\Reviews\BeforeAddPerformerReviewEvent;
use App\Events\Order\Show\Bids\BeforeChoosePerformer;
use App\Events\Order\Show\Bids\BidBeforeDisabledEvent;
use App\Events\Order\Show\Bids\BidCreatedEvent;
use App\Events\Order\Show\Bids\BidDisabledEvent;
use App\Events\Order\Show\Bids\BidEditedEvent;
use App\Events\Order\Show\Bids\ChoosedPerformer;
use App\Events\Order\Show\OrderViewedEvent;
use App\Events\Phone\PhoneVerifiedEvent;
use App\Events\Phone\VerificationGettedEvent;
use App\Events\Platform\Cabinet\UserTypeSwitched;
use App\Events\Platform\User\Analytics\UserLastSeedUpdated;
use App\Events\User\Registration\PhoneVerifyStepSuccessCompletedEvent;
use App\Events\User\Registration\UserSelectTypeSelectedEvent;
use App\Listeners\Admin\Menu\DropMenuCache;
use App\Listeners\Admin\User\OnUserAuth;
use App\Listeners\Order\Create\RegisteredUserCheckTempOrder;
use App\Listeners\Order\OrderCreated\MakeOrderSlugListener;
use App\Listeners\Order\OrderCreated\PublishOrderListener;
use App\Listeners\Order\OrderCreatedDetectPosterImageFromUploadedFilesListener;
use App\Listeners\Order\RecalcBidsListener;
use App\Listeners\Order\Review\CalculateAVGRatingReviewListener;
use App\Listeners\Order\Review\CheckToNeedsPublishReviews;
use App\Listeners\Order\Review\UserRatingChangedListener;
use App\Listeners\Order\Show\Bids\BidCreatedListener;
use App\Listeners\Order\Show\Bids\BidEditedListener;
use App\Listeners\Order\Show\OnOrderViewedIncrementOrderViews;
use App\Listeners\Phone\PhoneVerifiedListener;
use App\Listeners\Phone\VerificationGettedListener;
use App\Listeners\Platform\Cabinet\UserTypeSwitchedListeners;
use App\Listeners\Platform\User\Analytics\UserLastSeedUpdatedListener;
use App\Listeners\User\Registration\OnUserRegistered;
use App\Listeners\User\Registration\PhoneVerifyStepSuccessCompletedListener;
use App\Listeners\User\Registration\UserRegisteredFirstStepListener;
use App\Listeners\User\Registration\UserSelectTypeSelectedListener;
use App\Listeners\View\ViewBookingLanguagesCompose;
use App\Listeners\View\ViewLanguagesCompose;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 * @see OrderWasMakedListener
 */
class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Login::class => [
			OnUserAuth::class,
		],
		MenusChanged::class => [
			DropMenuCache::class,
		],
		\App\Events\Admin\Image\ImageUploaded::class => [
//            \App\Listeners\Admin\Image\ImageUploadedListener::class,
		],
		\App\Events\Admin\Image\MultipleImageUploaded::class => [
//            \App\Listeners\Admin\Image\MultipleImageUploadedListener::class,
		],
		'creating: public.layout.app' => [
			ViewBookingLanguagesCompose::class,
			ViewLanguagesCompose::class
		],


	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();
	}
}
