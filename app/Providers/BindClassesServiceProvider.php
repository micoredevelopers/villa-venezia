<?php

namespace App\Providers;

use App\Platform\Cabinet\Display\Performer\Tips\PerformerCompleteProfileTipHelper;
use Illuminate\Support\ServiceProvider;

class BindClassesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->when(PerformerCompleteProfileTipHelper::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
