<?php


	namespace App\Services\Phone;


	/**
	 * Class PhoneVerificationDataKeeper
	 * @package App\Services\Phone
	 * Class needed for save last verification code
	 */
	class PhoneVerificationDataKeeper
	{
		private $prefix = 'verification.session.';

		private $phone = '';

		private $code = '';

		public function __construct()
		{
			$this->setCode((string)$this->getData('code'));
			$this->setPhone((string)$this->getData('phone'));
		}

		/**
		 * @return string
		 */
		public function getPhone(): string
		{
			return $this->phone;
		}

		/**
		 * @param string $phone
		 */
		public function setPhone(string $phone): void
		{
			$this->storeData('phone', $phone);
			$this->phone = $phone;
		}

		/**
		 * @return string
		 */
		public function getCode(): string
		{
			return $this->code;
		}

		/**
		 * @param string $code
		 */
		public function setCode(string $code): void
		{
			$this->storeData('code', $code);
			$this->code = $code;
		}

		private function storeData(string $key, $data)
		{
			$prefix = $this->prefix;
			session()->put($prefix . $key, $data);
		}

		private function getData(string $key)
		{
			$prefix = $this->prefix;
			return session($prefix . $key);
		}

		private function removeData(string $key)
		{
			$prefix = $this->prefix;
			session()->forget($prefix . $key);
		}

		public function drop()
		{
			session()->remove('verification');
		}

	}