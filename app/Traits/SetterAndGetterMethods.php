<?php


	namespace App\Traits;


	trait SetterAndGetterMethods
	{
		protected function hasSetter(string $key)
		{
			return method_exists($this, $this->getNameSetter($key));
		}

		protected function hasGetter(string $key)
		{
			return method_exists($this, $this->getNameGetter($key));
		}

		protected function getNameGetter(string $key, $prefix = '')
		{
			return 'get' . \Str::studly($key) . $prefix;
		}

		protected function getNameSetter(string $key, $prefix = '')
		{
			return 'set' . \Str::studly($key) . $prefix;
		}

		protected function getPostfix()
		{
			if ($this->getSetPostfix ?? false) {
				return $this->getSetPostfix;
			}
			return '';
		}

		protected function _setProperty($key, $value)
		{
			$this->{$this->getNameSetter($key)}($value);
		}

		protected function _getProperty($key)
		{
			if ($this->hasGetter($key)){
				return $this->{$this->getNameGetter($key)}();
			}
			return null;
		}
	}