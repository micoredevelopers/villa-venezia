<?php


namespace App\Traits\DataKeepers;


trait StoreToSessionTrait
{

	private function storeData(string $key, $data): void
	{
		$prefix = $this->getPrefix();
		session()->put($prefix . $key, $data);
	}

	private function getData(string $key)
	{
		$prefix = $this->getPrefix();
		return session($prefix . $key);
	}

	private function getPrefix(): string
	{
		return ($this->prefix ?? false) ? ($this->prefix ?? '') . '.' : '';
	}
}