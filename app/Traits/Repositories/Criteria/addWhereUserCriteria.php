<?php


namespace App\Traits\Repositories\Criteria;


use App\Models\User;
use App\Repositories\AbstractRepository;
use Illuminate\Database\Eloquent\Builder;

trait addWhereUserCriteria
{
	public function addUserCriteria(User $user): self
	{
		/** @var $repository AbstractRepository*/
		$repository = $this;
		$repository->scopeQuery(function ($builder) use ($user) {
			return $builder->where($user->getForeignKey(), $user->getKey());
		});
		return $this;
	}
}