<?php

namespace App\Traits\Models\User;


use Illuminate\Support\Carbon;

trait UserMutatorTrait
{
	public function getBalance(): int
	{
		return (int)$this->getAttribute('balance');
	}

	public function getName($field = 'name'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getSurname($field = 'surname'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getFio(): string
	{
		return $this->getName() . ' ' . $this->getSurname();
	}

	public function getPhone($field = 'phone'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getEmail($field = 'email'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getAvatar($field = 'avatar'): string
	{
		return (string)$this->getAttribute($field);
	}

	public function getPhoneDisplay($field = 'phone'): string
	{
		return extractDigits($this->getAttribute($field));
	}

	public function getDateLastSeen(): ?Carbon
	{
		$date = $this->getAttribute('last_seen_at');
		return isDateValid($date) ? getDateCarbon($date) : null;
	}

}
