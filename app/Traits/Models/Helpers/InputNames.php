<?php


namespace App\Traits\Models\Helpers;


trait InputNames
{
	/**
	 * @param string $input
	 * @return string
	 * generate string like: news[3]['url']
	 */
	public function getNameInputByKey(string $input)
	{
		return $this->getNameInput() . '[' . $input . ']';
	}

	/**
	 * @param string $input
	 * @return mixed
	 * @example $name = $request->input($model->getNameInputRequestKey('name'))
	 * @generate string like: news.3.url
	 */
	public function getNameInputRequestByKey(string $input)
	{
		return remakeInputKeyDotted($this->getNameInputByKey($input));
	}

	/**
	 * @return mixed
	 * @example $key = $model->getNameInputRequest()
	 * generate string like: news.3
	 */
	public function getNameInputRequest()
	{
		return remakeInputKeyDotted($this->getNameInput());
	}

	/**
	 * @return string
	 * generate string like: news[3]
	 */
	public function getNameInput()
	{
		$primary = ($primaryValue = $this->getKey()) ? '[' . $primaryValue . ']' : '';
		return $this->getTable() . $primary;
	}

}