<?php

namespace App\Traits\Models\Localization;

trait RedirectLangColumn
{

	public function getAttribute($key)
	{
		if (in_array($key, $this->langColumns ?? [],true)) {
			return $this->getLangColumn($key);
		}
		return parent::getAttribute($key);
	}
}