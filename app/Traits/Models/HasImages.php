<?php

namespace App\Traits\Models;

use App\Models\Image;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

trait HasImages
{
    /**
     * @return mixed
     */
	public function images():MorphMany
	{
		return $this->morphMany('App\Models\Image', 'imageable');
	}

	/**
	 * @return Collection | Image[]
	 */
	public function getImages():Collection
	{
		return $this->images;
	}

}