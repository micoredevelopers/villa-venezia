<?php

namespace App\Traits\Models;


use App\Contracts\HasLocalized;

trait MainOrLangNameAttributeTrait
{

    /**
     * @param string $column
     * @return mixed|string
     */
    public function getNameAttribute()
    {
    	$column = 'name';
        return $this->getLangColumn($column);
    }

}