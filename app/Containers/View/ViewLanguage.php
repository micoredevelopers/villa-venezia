<?php declare(strict_types=1);

namespace App\Containers\View;

final class ViewLanguage implements ViewLanguageInterface
{
	private $isActive;
	private $name;
	private $url;
	private $key;
	private $isoKey;

	/**
	 * ViewLanguage constructor.
	 * @param string $name
	 * @param string $url
	 * @param string $key
	 * @param string $isoKey
	 * @param bool $isActive
	 */
	public function __construct(
		string $name,
		string $url,
		string $key,
		string $isoKey,
		bool $isActive = false
	)
	{
		$this->name = $name;
		$this->url = $url;
		$this->key = $key;
		$this->isoKey = $isoKey;
		$this->isActive = $isActive;
	}

	public function getIsActive(): bool
	{
		return $this->isActive;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getUrl(): string
	{
		return $this->url;
	}

	public function getKey(): string
	{
		return $this->key;
	}

	public function getIsoKey(): string
	{
		return $this->isoKey;
	}

}