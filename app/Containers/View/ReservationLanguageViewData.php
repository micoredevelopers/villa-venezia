<?php declare(strict_types=1);

namespace App\Containers\View;

final class ReservationLanguageViewData implements ReservationLanguageViewDataInterface
{
	private $currentLanguageCode;

	private $supportedLocales = [];

	/**
	 * @return mixed
	 */
	public function getCurrentLanguageCode(): string
	{
		return $this->currentLanguageCode;
	}

	/**
	 * @param mixed $currentLanguageCode
	 */
	public function setCurrentLanguageCode($currentLanguageCode): void
	{
		$this->currentLanguageCode = $currentLanguageCode;
	}

	/**
	 * @return array
	 */
	public function getSupportedLocales(): array
	{
		return $this->supportedLocales;
	}

	/**
	 * @param array $supportedLocales
	 */
	public function setSupportedLocales(array $supportedLocales): void
	{
		$this->supportedLocales = $supportedLocales;
	}
}