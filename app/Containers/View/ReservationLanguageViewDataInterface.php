<?php declare(strict_types=1);

namespace App\Containers\View;

interface ReservationLanguageViewDataInterface
{
	public function getCurrentLanguageCode(): string;

	public function getSupportedLocales(): array;
}