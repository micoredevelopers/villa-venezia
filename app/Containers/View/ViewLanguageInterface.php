<?php declare(strict_types=1);

namespace App\Containers\View;

interface ViewLanguageInterface
{
	public function getIsActive(): bool;

	public function getName(): string;

	public function getUrl(): string;

	public function getKey(): string;

	public function getIsoKey(): string;
}