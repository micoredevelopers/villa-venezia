<?php

use Illuminate\Support\Str;

if (!function_exists('isStringUrl')) {
	function isStringUrl($string)
	{
		$http = Str::startsWith($string, 'http://');
		$https = Str::startsWith($string, 'https://');
		$double = Str::startsWith($string, '//');
		return ($http or $https or $double);
	}
}

if (!function_exists('langUrl')) {
	function langUrl($url, $locale = false): string
	{
		$localeCode = $locale ?: getCurrentLocale();

		return \LaravelLocalization::getLocalizedURL($localeCode, $url, [], false);
	}
}

if (!function_exists('getNonLocaledUrl')) {
	function getNonLocaledUrl($url = null)
	{
		if ($url === null) {
			$url = request()->getPathInfo();
		}

		return \LaravelLocalization::getNonLocalizedURL($url);
	}
}

if (!function_exists('urlWithoutPublic')) {
	function urlWithoutPublic($url)
	{
		return Str::replaceFirst('/public', '', $url);
	}
}

if (!function_exists('getUrlWithoutHost')) {
	function getUrlWithoutHost($url)
	{
		$domain = env('APP_URL');
		$url = urlWithoutPublic($url);
		return Str::replaceFirst($domain, '', $url);
	}
}

if (!function_exists('isLink')) {
	function isLink($str = null)
	{
		return filter_var($str, FILTER_VALIDATE_URL);
	}
}

function urlEntityEdit(\App\Models\Model $model, $action = 'edit')
{
	try {
		return CRUDLinkByModel($model)->{$action}();
	} catch (\Exception $e) {
		return '';
	}
}


function routeKeys(string $module)
{
	$prepend = '';
	$keys = [
		'sitemap'   => 'admin.sitemap',
		'robots'    => 'admin.robots',
		'users'     => 'admin.users',
		'roles'     => 'admin.roles',
		'settings'  => 'admin.settings',
		'translate' => 'admin.translate',
		'meta'      => 'admin.meta',
		'menus'     => 'admin.menu',
		//
		'pages'     => 'admin.pages',
		'news'      => 'admin.news',
		'feedback'  => 'admin.feedback',
		'sliders'   => 'admin.sliders',
		'faqs'      => 'admin.faq',
		'orders'    => 'admin.orders',
	];

	return $prepend . ((string)Arr::get($keys, $module));
}


function isMainPage($fromCache = true)
{
	static $cache;
	if (null === $cache || !$fromCache) {
		$cache = \Illuminate\Support\Facades\Route::is('home');
	}
	return $cache;
}

function getUrlByPage(string $type): string
{
	$pages = [
		'about'        => langUrl('/about'),
		'how-it-works' => langUrl('/how-it-works'),
		'faq'          => langUrl('/faq'),
		'report'       => langUrl('/report'),
		'idea'         => langUrl('/idea'),
		'oferta'       => langUrl('/oferta'),
		'privacy'      => langUrl('/privacy'),
	];
	return $pages[$type] ?? '';
}