<?php

	use App\Models\Translate\Translate;

	if (!function_exists('getTranslate')) {
		function getTranslate($key, $defaultText = '', $asObject = false)
		{

			\App\Helpers\Sidebar\PageUsedTranslateHelper::addTranslate($key);
			return Translate::getTranslate($key, $asObject) ?? $defaultText ?: $key;
		}
	}
	if (!function_exists('translateFormat')) {
		function translateFormat($key, array $values)
		{
			/** @var $translate Translate */
			$translate = getTranslate($key, false, true);
			if ($translate instanceof Translate) {
				return str_replace(array_keys($values), array_values($values), $translate->value);
			}
			return '';
		}
	}
	if (!function_exists('translateYesNo')) {
		function translateYesNo($condition)
		{
			return $condition ? getTranslate('global.yes') : getTranslate('global.no');
		}
	}