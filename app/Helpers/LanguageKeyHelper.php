<?php declare(strict_types=1);

namespace App\Helpers;

final class LanguageKeyHelper
{
	public static function getIso(string $key): string
	{
		$replace = [
			'ua' => 'uk',
		];

		return $replace[$key] ?? $key;
	}
}