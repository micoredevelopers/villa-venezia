<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\Assets;
use App\Traits\Controllers\Breadcrumbs;
use App\Traits\Controllers\HasMessages;
use App\Traits\Controllers\SEOMeta;

abstract class BaseController extends Controller
{
    use HasMessages;
    use Breadcrumbs;
    use SEOMeta;
    use Assets;

    public function __construct() {}
}











