<?php

namespace App\Http\Controllers;

class ReservationController extends SiteController
{

	public function index()
	{
		$this->setTitle(getTranslate('reservation.title', 'Бронирование'));
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('public.reservation.reservation')->with($with);
		return $this->main($data);
	}


}
