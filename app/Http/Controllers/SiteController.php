<?php

	namespace App\Http\Controllers;

	use App\Models\Meta;
	use App\Repositories\MenuRepository;
	use Illuminate\Contracts\Pagination\LengthAwarePaginator;
	use Illuminate\Support\Str;
	use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

	abstract class SiteController extends BaseController
	{
		protected $lang;
		/**
		 * @var MenuRepository
		 */
		private $menuRepository;

		public function __construct()
		{
			parent::__construct();
			$this->addBreadCrumb(getSetting('global.home-bread-name'), route('home'));
			$this->lang = getCurrentLangId();
		}

		public function main($data = [])
		{
			/** @var  $view */
			$menus = app(MenuRepository::class)->getMenus()['main_menu'];
			$with = compact(array_keys(get_defined_vars()));
			$data = array_merge($data, $with);

			$view = view('public.layout.app', $data);
			return $view;
		}

		public function callAction($method, $parameters)
		{
			$res = parent::callAction($method, $parameters);
			$this->postCallAction();
			return $res;
		}

		protected function postCallAction()
		{
			$this->checkMetaData();
		}

		public function checkMetaData(): void
		{
			if ($metadata = Meta::getMetaData()) {
				if ($metadata->title) {
					$this->setTitle($metadata->title, true);
				}
				if ($metadata->description) {
					$this->setDescription($metadata->description, true);
				}
				if ($metadata->keywords) {
					$this->setKeywords($metadata->keywords, true);
				}
				return;
			}
			if ((!$this->getTitle() || !$this->getDescription() || !$this->getKeywords()) && ($meta_default = Meta::getDefaultMeta())) {
				if (!$this->getTitle()) {
					$this->setTitle($meta_default->title);
				}
				if (!$this->getDescription()) {
					$this->setDescription($meta_default->description);
				}
				if (!$this->getKeywords()) {
					$this->setKeywords($meta_default->keywords);
				}
			}
		}


	}











