<?php

	namespace App\Http\Controllers;


	use App\Repositories\ReviewRepository;
	use App\Repositories\RoomRepository;
	use App\Repositories\ServiceRepository;

	class HomeController extends SiteController
	{
		/**
		 * @var ServiceRepository
		 */
		private $serviceRepository;
		/**
		 * @var RoomRepository
		 */
		private $roomRepository;
		/**
		 * @var ReviewRepository
		 */
		private $reviewRepository;

		public function __construct(ServiceRepository $serviceRepository, RoomRepository $roomRepository, ReviewRepository $reviewRepository)
		{
		    parent::__construct();
			$this->serviceRepository = $serviceRepository;
			$this->roomRepository = $roomRepository;
			$this->reviewRepository = $reviewRepository;
		}

		public function index()
		{
			$this->setTitle(getTranslate('main.title'));

			$services = $this->serviceRepository->getListPublic();
			$rooms = $this->roomRepository->getListPublic();
			$rooms->load('images');
			$reviews = $this->reviewRepository->getListPublic();
			$with = compact(array_keys(get_defined_vars()));
			$data['headerClass'] = 'light';
			$data['content'] = view('public.home.home')->with($with);
			return $this->main($data);
		}
	}
