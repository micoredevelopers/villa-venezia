<?php

	namespace App\Http\Controllers;

	use App\Repositories\FaqRepository;
	use App\Repositories\MenuRepository;
	use App\Repositories\RoomRepository;

	class PageController extends SiteController
	{

		public function about(RoomRepository $roomRepository)
		{
			$this->setTitle(getTranslate('about.title', 'О нас'));
			$rooms = $roomRepository->getListPublic();
			$with = compact(array_keys(get_defined_vars()));
			$data['content'] = view('public.pages.about')->with($with);
			return $this->main($data);
		}


		public function contacts()
		{
			$this->setTitle(getTranslate('contacts.title', 'Контакты'));
			$with = compact(array_keys(get_defined_vars()));
			$data['content'] = view('public.pages.contacts')->with($with);
			return $this->main($data);
		}


	}
