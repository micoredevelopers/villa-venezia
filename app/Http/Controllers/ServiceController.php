<?php

	namespace App\Http\Controllers;

	use App\Repositories\ServiceRepository;

	class ServiceController extends SiteController
	{
		public function index(ServiceRepository $serviceRepository)
		{
			$this->setTitle(getTranslate('services.title'));
			$services = $serviceRepository->getListPublic();
			$with = compact(array_keys(get_defined_vars()));
			$data['content'] = view('public.services.services')->with($with);
			return $this->main($data);
		}


	}
