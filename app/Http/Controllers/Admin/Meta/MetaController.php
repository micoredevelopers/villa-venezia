<?php

namespace App\Http\Controllers\Admin\Meta;

use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\MetaRequest;
use App\Models\Meta;
use App\Models\MetaLang;
use App\Repositories\MetaRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;

class MetaController extends AdminController
{

	use Authorizable;

	protected $key = 'meta';
	protected $routeKey = 'admin.meta';
	protected $permissionKey = 'meta';
	protected $name = 'SEO';

	public function __construct()
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
	}


	public function index(MetaRepository $metaRepository, Request $request)
	{
		\View::share('request', $request);
		$this->setTitle($this->name);
		$vars['list'] = $metaRepository->getForAdminDisplay($request);
		$data['content'] = view('admin.meta.index', $vars);

		return $this->main($data);
	}

	public function create(Request $request)
	{
		if ($request->has('url')) {
			$metaExisted = Meta::findByUrl(Meta::makeUrlClear($request->get('url')));
			if ($metaExisted) {
				return redirect($this->resourceRoute('edit', $metaExisted->id));
			}
		}
		$data['content'] = view('admin.meta.create');

		return $this->main($data);
	}

	public function store(MetaRequest $request, Meta $meta)
	{
		$input = $request->all();
		$this->setSuccessStore();
		if ($meta->fill($input)->save()) {
			foreach ($this->languagesList as $language) {
				$metaLang = new MetaLang();
				$metaLang->associateWithLanguage($language);
				$metaLang->meta()->associate($meta);
				$metaLang->fill($input)->save();
			}
			$this->setSuccessStore();
		}
		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $meta->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		$edit = Meta::with('lang')->findOrFail($id);
		$title = $this->titleEdit($edit, 'url');
		$this->setTitle($title)->addBreadCrumb($title);
		$data['content'] = view('admin.meta.edit', compact('edit'));

		return $this->main($data);
	}

	public function update(MetaRequest $request, Meta $meta)
	{
		d($meta);
		$input = $request->all();
		if (!isSuperAdmin()) {
			unset($input['url']);
		}
		$meta->fill($input);
		if ($meta->save()) {
			$meta->lang->fill($input)->save();
			$this->setSuccessUpdate();
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}

	public function destroy(Meta $meta)
	{
		if ($meta->delete()) {
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}
}
