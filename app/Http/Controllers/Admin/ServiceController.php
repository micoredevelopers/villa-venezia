<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Controllers\Admin\AdminController;
	use App\Http\Requests\Admin\ServiceRequest;
	use App\Models\Service\Service;
	use App\Repositories\ServiceRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;

	class ServiceController extends AdminController
	{
		protected $withThumbnails = true;

		use Authorizable;
		use SaveImageTrait;

		private $moduleName;

		protected $key = 'services';

		protected $routeKey = 'admin.services';

		protected $permissionKey = 'services';
		/**
		 * @var ServiceRepository
		 */
		private $repository;

		public function __construct(ServiceRepository $repository)
		{
			parent::__construct();
			$this->moduleName = __('modules.services.title');
			$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->moduleName);
			$vars['list'] = $this->repository->getListAdmin();
			$data['content'] = view('admin.services.index', $vars);
			return $this->main($data);
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function create()
		{
			$data['content'] = view('admin.services.create');
			return $this->main($data);
		}

		public function store(ServiceRequest $request)
		{
			$input = $request->except('image', 'icon');
			if ($service = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->saveImage($request, $service);
				$this->saveImage($request, $service, 'icon');
				$this->fireEvents();
			}
			return $this->redirectOnCreated($service);
		}

		public function edit(Service $service)
		{
			$service->load('lang');
			$edit = $service;
			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);
			$data['content'] = view('admin.services.edit', compact(
				'edit'
			));
			return $this->main($data);
		}

		public function update(ServiceRequest $request, Service $service)
		{
			$input = $request->only($request->getFillableFields(['image', 'icon']));
			$this->saveImage($request, $service);
			$this->saveImage($request, $service, 'icon');

			if ($this->repository->update($input, $service)) {
				$this->setSuccessUpdate();
				$this->fireEvents();
			}

			return $this->redirectOnUpdated($service);
		}

		/**
		 * @param Service $service
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 * @throws \Exception
		 */
		public function destroy(Service $service, ImageRemover $imageRemover)
		{
			if (!$service->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($service->id)) {
				$imageRemover->removeImage($service->image);
				$this->setSuccessDestroy();
				$this->fireEvents();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		private function fireEvents()
		{
		}
	}
