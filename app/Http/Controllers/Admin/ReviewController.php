<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Media\ImageRemover;
use App\Http\Requests\Admin\ReviewRequest;
use App\Models\Review;
use App\Repositories\ReviewRepository;
use App\Traits\Authorizable;

class ReviewController extends AdminController
{

	use Authorizable;

	private $moduleName;

	protected $key = 'reviews';

	protected $routeKey = 'admin.reviews';

	protected $permissionKey = 'reviews';
	/**
	 * @var ReviewRepository
	 */
	private $repository;

	public function __construct(ReviewRepository $repository)
	{
		parent::__construct();
		$this->moduleName = __('modules.reviews.title');
		$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index()
	{
		$this->setTitle($this->moduleName);
		$vars['list'] = $this->repository->getListAdmin();
		$data['content'] = view('admin.reviews.index', $vars);
		return $this->main($data);
	}

	public function edit(Review $review)
	{
		$edit = $review;
		$with = compact(array_keys(get_defined_vars()));

		$title = $this->titleEdit($edit);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.reviews.edit')->with($with);
		return $this->main($data);
	}

	public function update(ReviewRequest $request, Review $review)
	{
		$input = $request->only($request->getFillableFields(['image']));

		if ($this->repository->update($input, $review)) {
			$this->setSuccessUpdate();
		}

		return $this->redirectOnUpdated($review);
	}


	public function destroy(Review $review, ImageRemover $imageRemover)
	{
		if (!$review->canDelete()) {
			$this->setFailMessage('Удаление этой записи не доступно');
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		if ($this->repository->delete($review->id)) {
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
