<?php

	namespace App\Http\Controllers\Admin;

	use App\Helpers\Media\ImageRemover;
	use App\Http\Requests\Admin\RoomRequest;
	use App\Models\Room\Room;
	use App\Repositories\RoomRepository;
	use App\Traits\Authorizable;
	use App\Traits\Controllers\SaveImageTrait;

	class RoomController extends AdminController
	{
		protected $withThumbnails = false;

		use Authorizable;
		use SaveImageTrait;

		private $moduleName;

		protected $key = 'rooms';

		protected $routeKey = 'admin.rooms';

		protected $permissionKey = 'rooms';
		/**
		 * @var RoomRepository
		 */
		private $repository;

		public function __construct(RoomRepository $repository)
		{
			parent::__construct();
			$this->moduleName = __('modules.rooms.title');
			$this->addBreadCrumb($this->moduleName, $this->resourceRoute('index'));
			$this->shareViewModuleData();
			$this->repository = $repository;
		}

		public function index()
		{
			$this->setTitle($this->moduleName);
			$vars['list'] = $this->repository->getListAdmin();
			$data['content'] = view('admin.rooms.index', $vars);
			return $this->main($data);
		}

		/**
		 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
		 */
		public function create()
		{
			$data['content'] = view('admin.rooms.create');
			return $this->main($data);
		}

		public function store(RoomRequest $request)
		{
			$input = $request->except('image');
			if ($room = $this->repository->create($input)) {
				$this->setSuccessStore();
				$this->saveImage($request, $room);
			}
			return $this->redirectOnCreated($room);
		}

		public function edit(Room $room)
		{
			$room->load('lang');
			$edit = $room;
			$photosList = $edit->images;
			$with = compact(array_keys(get_defined_vars()));

			$title = $this->titleEdit($edit);
			$this->addBreadCrumb($title)->setTitle($title);
			$data['content'] = view('admin.rooms.edit')->with($with);
			return $this->main($data);
		}

		public function update(RoomRequest $request, Room $room)
		{
			$input = $request->only($request->getFillableFields(['image']));
			$this->saveImage($request, $room);

			if ($this->repository->update($input, $room)) {
				$this->saveAdditionalImages($room, $request);
				$this->setSuccessUpdate();
			}

			return $this->redirectOnUpdated($room);
		}

		/**
		 * @param Room $room
		 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
		 * @throws \Exception
		 */
		public function destroy(Room $room, ImageRemover $imageRemover)
		{
			if (!$room->canDelete()) {
				$this->setFailMessage('Удаление этой записи не доступно');
				return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
			}
			if ($this->repository->delete($room->id)) {
				$imageRemover->removeImage($room->image);
				$this->setSuccessDestroy();
			}

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

	}
