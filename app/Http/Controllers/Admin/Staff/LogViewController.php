<?php

namespace App\Http\Controllers\Admin\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider;

class LogViewController extends \Rap2hpoutre\LaravelLogViewer\LogViewerController
{
	public function index()
	{
		\Gate::authorize('view_logs');
		return parent::index();
	}
}
