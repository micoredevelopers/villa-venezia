<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Debug\LoggerHelper;
use App\Models\Language;
use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use App\Traits\Authorizable;
use Database\Seeders\TranslateTableSeeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TranslateController extends AdminController
{
	use Authorizable;

	private $name;
	protected $tb;

	protected $routeKey = 'translate';

	protected $permissionKey = 'translate';
	/**
	 * @var TranslateRepository
	 */
	private $repository;

	public function __construct(TranslateRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.localization.title');
		$this->tb = 'translate';
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->addScripts([
			'js/lib/select2/select2.full.min.js',
			'js/lib/select2/ru.js',
		]);
		$this->addCss([
			'css/lib/select2.min.css',
		]);
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	/**
	 * @param Request $request
	 * @param TranslateRepository $translateRepository
	 * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
	 */
	public function index(Request $request, TranslateRepository $translateRepository)
	{
		if ($request->has('seed')) {
			try {
				seedByClass('TranslateTableSeeder');
			} catch (\Throwable $e) {
				$this->setFailMessage($e->getMessage());
			}
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		$data = [];
		$vars['list'] = $translateRepository->getForAdminDisplay($request);

		$groups = $vars['list']->pluckDistinct('group');
		$groups = collect($groups);
		$vars = array_merge($vars, compact('request'));
		$this->setTitle($this->name);
		$active = request()->session()->get('setting_tab', old('setting_tab', ($groups->first())));
		$vars = array_merge($vars, compact(
			'active'
			, 'groups'
		));
		if (view()->exists('admin.translate.index')) {
			$data['content'] = view('admin.translate.index', $vars);
		}

		return $this->main($data);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$vars['groups'] = Translate::getGroups();
		$this->addBreadCrumb(__('form.creating'));
		$vars['controller'] = $this->tb;
		$data['content'] = view('admin.translate.add')->with($vars);
		return $this->main($data);
	}

	public function store(Request $request)
	{
		$data = $request->all();

		if ($translate = $this->repository->create($data)) {
			$this->setSuccessStore();
			return $this->redirectOnCreated($translate);
		}
		return redirect()->back()->with($this->getResponseMessage());

	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update(Request $request)
	{
		if ($request->has('translate')) {
			$ids = array_keys($request->get('translate'));
			$this->setSuccessUpdate();
			$translates = $this->repository->with('lang')->find($ids);
			$translateRequest = $request->get('translate');
			foreach ($translates as $translate) {
				/** @var $translate Translate */
				try {
					$id = $translate->getKey();
					$data = \Arr::get($translateRequest, $id);
					$this->repository->update($data, $translate);
					if (!$translate->wasChanged() && $translate->lang->wasChanged()) {
						$translate->touch();
					}
				} catch (\Exception $e) {
					app(LoggerHelper::class)->error($e);
				}
			}
		}
		request()->flashOnly('setting_tab');
		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());

	}

}
