<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddReviewRequest;
use App\Repositories\ReviewRepository;

class ReviewController extends SiteController
{
	public function store(AddReviewRequest $request, ReviewRepository $reviewRepository)
	{
		$data = $request->only($request->getFillableFields());
		$data['active'] = false;
		try{
			$reviewRepository->create($data);
			$this->setSuccessMessage(getTranslate('reviews.add.success', 'Ваш отзыв успешно добавлен'));
		} catch (\Throwable $e){
			$this->setFailMessage(getTranslate('reviews.add.fail'));
		}
		$this->setResponseData(['successReview' => true]);
		return redirect(route('home'))->with($this->getResponseMessageForJson());
	}


}
