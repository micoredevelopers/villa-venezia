<?php

namespace App\Http\Requests\Admin;

use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;

class ReviewRequest extends AbstractRequest
{
	protected $toBooleans = ['active'];
	protected $fillableFields = ['active'];

	public function rules(): array
	{
		$rules = [
			'name'   => ['required', 'string', 'max:255'],
			'review' => ['string', 'max:' . ValidationMaxLengthHelper::TEXT],
		];
		return $rules;
	}

}
