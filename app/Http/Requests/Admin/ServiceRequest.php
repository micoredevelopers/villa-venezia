<?php

	namespace App\Http\Requests\Admin;

	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;

	class ServiceRequest extends AbstractRequest
	{
		protected $toBooleans = ['active', 'free'];

		protected $fillableFields = ['free', 'active'];

		public function rules(): array
		{
			$rules = [
				'name'        => ['required', 'string', 'max:255'],
				'excerpt'     => ['max:' . ValidationMaxLengthHelper::TEXT],
				'description' => ['max:' . ValidationMaxLengthHelper::TEXT],
				'image'       => ['nullable', 'image'],
				'icon'        => ['nullable', 'image'],
			];
			return $rules;
		}

	}
