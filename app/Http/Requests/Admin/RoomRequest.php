<?php

	namespace App\Http\Requests\Admin;

	use App\Helpers\Validation\ValidationMaxLengthHelper;
	use App\Http\Requests\AbstractRequest;

	class RoomRequest extends AbstractRequest
	{

		public function rules(): array
		{
			$rules = [
				'name'        => ['required', 'string', 'max:255'],
				'excerpt'     => ['max:' . ValidationMaxLengthHelper::TEXT],
				'description' => ['max:' . ValidationMaxLengthHelper::TEXT],
				'image'       => ['nullable', 'image'],
				'price'       => ['required', 'numeric', 'max:' . ValidationMaxLengthHelper::INT],
			];
			return $rules;
		}

	}
