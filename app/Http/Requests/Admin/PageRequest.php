<?php

namespace App\Http\Requests\Admin;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Traits\Requests\Helpers\GetActionModel;

class PageRequest extends AbstractRequest implements RequestParameterModelable
{
    use GetActionModel;

    protected $toBooleans = ['active', 'manual'];

    protected $requestKey = 'page';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title'       => 'required|max:' . ValidationMaxLengthHelper::CHAR,
            'url'         => ['required', 'unique:pages,url', 'max:160'],
            'description' => ['max:' . ValidationMaxLengthHelper::MEDIUMTEXT],
            'excerpt'      => ['max:' . ValidationMaxLengthHelper::TEXT],
        ];
        if ($parameterModel = $this->getActionModel()) {
            $rules['url'] = ['required', 'unique:pages,url,' . $parameterModel->id, 'max:160'];
        }
        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->mergeUrlFromTitle();
    }
}
