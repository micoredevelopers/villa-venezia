<?php

namespace App\Http\Requests;

use App\Helpers\Validation\ValidationMaxLengthHelper;

class AddReviewRequest extends AbstractRequest
{
	protected $fillableFields = ['ip'];

	public function rules()
	{
		return [
			'name'   => ['required', 'string', 'max:255'],
			'review' => ['string', 'max:' . ValidationMaxLengthHelper::TEXT],
		];
	}

	protected function prepareForValidation()
	{
		$this->merge(['ip' => $this->getClientIp()]);
	}
}
