<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\AbstractRequest;
use App\Rules\Feedback\FeedbackThrottle;

class FileFeedbackRequest extends AbstractRequest
{
	protected $fillableFields = ['type', 'ip'];

	public function rules()
	{
		return [
			'name'    => $this->getRuleRequiredChar(),
			'message' => array_merge($this->getRuleRequiredChar(), ['max:2000']),
			'email'   => $this->getBaseEmailRule(),
			'files.*' => array_merge($this->getFilesRule(), [app(FeedbackThrottle::class)]),
			'files'   => 'max:20',
		];
	}

	protected function mergeRequestValues()
	{
		$this->merge(['ip' => $this->ip()]);
	}

	public function messages()
	{
		return [
			'files.max' => 'Максимум :max файлов',
		];
	}
}
