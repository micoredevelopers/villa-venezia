<?php

	namespace App\Http\Middleware\Admin;

	use Closure;
	use Illuminate\Http\Request;
	use Illuminate\Support\Str;

	class RedirectAdminPath
	{
		private $defaultPath = '/admin';

		/**
		 * Handle an incoming request.
		 *
		 * @param \Illuminate\Http\Request $request
		 * @param \Closure $next
		 * @return mixed
		 */
		public function handle($request, Closure $next)
		{
			if ($this->isNeedRedirect($request)) {
				$newUrl = $this->getUrlRedirect($request);
				return redirect($newUrl)->withInput();
			}
			return $next($request);
		}

		private function isNeedRedirect(Request $request): bool
		{
			$startsWithAdmin = Str::startsWith($request->getRequestUri(), $this->defaultPath);
			$configPath = '/' . trim(\Config::get('app.admin-url'), '/');
			return $startsWithAdmin && ($this->defaultPath !== $configPath);
		}

		private function getUrlRedirect(Request $request): string
		{
			return  Str::replaceFirst($this->defaultPath, \Config::get('app.admin-url'), $request->getRequestUri());
		}
	}
