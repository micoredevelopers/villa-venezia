<?php

	namespace App\Models;

	class City extends Model
	{
		protected $guarded = ['id'];

		public function getName()
		{
			return $this->name ?? '';
		}
	}
