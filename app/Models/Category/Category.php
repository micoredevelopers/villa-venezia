<?php

namespace App\Models\Category;

use \App\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class Category extends Model
{
	protected $guarded = ['id'];

	public function categories()
	{
		return $this->hasMany(self::class, 'parent_id');
	}

	public function parent():BelongsTo
	{
		return $this->belongsTo(self::class, 'parent_id');
	}

	public function getNameDisplay(): ?string
	{
		return (string)$this->getAttribute('name');
	}

	public function getDescription()
	{
		return  $this->getAttribute('description');
	}

	public function getSlug()
	{
		return $this->getKey();
	}

	/**
	 * @return Collection | static[]
	 */
	public function getSubcategories(): Collection
	{
		return $this->categories;
	}

	/**
	 * @param Collection | Category[] $collection
	 * @return self
	 */
	public function setSubcategories(Collection $collection): self
	{
		$this->setRelation('categories', $collection);
		return $this;
	}

	public function hasSubcategories(): bool
	{
		return $this->getSubcategories()->isNotEmpty();
	}
}
