<?php

	namespace App\Models\Service;


	use App\Models\ModelLang;

	class ServiceLang extends ModelLang
	{

		protected $table = 'service_lang';

		protected $primaryKey = ['service_id', 'language_id'];

		public function service()
		{
			return $this->belongsTo(Service::class);
		}

	}
