<?php

	namespace App\Models\Service;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;

	class Service extends Model implements HasLocalized
	{

		use RedirectLangColumn;
		use ImageAttributeTrait;

		protected $hasOneLangArguments = [ServiceLang::class];
		protected $langColumns = ['name', 'description', 'excerpt'];

		protected $guarded = ['id'];

	}
