<?php

	namespace App\Models\Page;


	use App\Contracts\HasImagesContract;
	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Models\Page\Traits\PageHelpers;
	use App\Traits\Models\HasImages;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Support\Arr;


	class Page extends Model implements HasImagesContract, HasLocalized
	{
		use RedirectLangColumn;
		use PageHelpers;
		use ImageAttributeTrait;
		use HasImages;

		protected $hasOneLangArguments = [PageLang::class];
		protected $langColumns = ['title', 'description', 'excerpt', 'sub_title', 'sub_description', 'name'];

		protected $casts = [
			'manual'  => 'boolean',
			'options' => 'array',
		];
		protected $guarded = [
			'id',
		];

		protected static $pageTypes = [
			'main'         => 'Main',
		];

		public function parent($withLang = false)
		{
			$query = $this->belongsTo(__CLASS__, 'parent_id', 'id');
			if ($withLang) {
				$query->with('lang');
			}
			return $query;
		}

		public static function getPageTypes()
		{
			return self::$pageTypes;
		}

		public function pageTypeExists($pageType)
		{
			return Arr::exists(self::$pageTypes, $pageType);
		}

		public function setUrlAttribute($url): self
		{
			if (!$this->getAttribute('manual')) {
				$url = \Str::slug($url);
			} else {
				$url = '/' . ltrim($url, '/');
			}
			$column = 'url';
			$this->attributes[ $column ] = $url;
			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getUrlAttribute()
		{
			$column = 'url';
			return \Arr::get($this->attributes, $column);
		}

		public function getRouteUrl()
		{
			try{
				if ($this->isManualLink()) {
					return url($this->getUrlAttribute());
				}
				return route('pages.show', $this->getUrlAttribute());
			} catch (\Exception $e){
			    app(\App\Helpers\Debug\LoggerHelper::class)->error($e);
			    return '';
			}
		}

		public function isManualLink()
		{
			return $this->getAttribute('manual');
		}

		public function setPageTypeAttribute($type)
		{
			if (!$this->pageTypeExists($type)) {
				$type = null;
			}
			$this->attributes['page_type'] = $type;
		}

	}
