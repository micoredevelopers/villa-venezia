<?php

	namespace App\Models;

	use App\Observers\VerificationObserver;
	use Illuminate\Support\Carbon;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	class Verification extends Model
	{
		protected $guarded = ['id'];

		public $timestamps = ['available_at'];

		public function getPhone(): string
		{
			return (string)$this->getAttribute('phone');
		}

		public function getCode(): string
		{
			return (string)$this->getAttribute('code');
		}

		public function setCode($code): self
		{
			$this->setAttribute('code', $code);
			return $this;
		}

		public function getAvailableAt(): Carbon
		{
			return getDateCarbon($this->getAttribute('available_at'));
		}

		public function isAvailable()
		{
			return $this->getAvailableAt() <= now();
		}

		public function getDiffInSeconds()
		{
			return $this->getAvailableAt()->diffInSeconds(now());
		}

		protected static function boot()
		{
			parent::boot();
			if (class_exists(VerificationObserver::class)) {
				static::observe(VerificationObserver::class);
			}

		}
	}
