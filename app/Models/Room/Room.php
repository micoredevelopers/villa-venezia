<?php

	namespace App\Models\Room;

	use App\Contracts\HasImagesContract;
	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Traits\Models\HasImages;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;

	class Room extends Model implements HasLocalized, HasImagesContract
	{
		use HasImages;
		protected $guarded = ['id'];

		use RedirectLangColumn;
		use ImageAttributeTrait;

		protected $hasOneLangArguments = [RoomLang::class];
		protected $langColumns = ['name', 'description', 'excerpt'];

	}
