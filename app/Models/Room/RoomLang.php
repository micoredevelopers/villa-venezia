<?php

	namespace App\Models\Room;

	use App\Models\ModelLang;

	class RoomLang extends ModelLang
	{
		protected $table = 'room_lang';

		protected $primaryKey = ['room_id', 'language_id'];

		public function service()
		{
			return $this->belongsTo(Room::class);
		}
	}
