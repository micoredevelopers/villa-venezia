<?php

namespace App\Models\Feedback;

interface FeedbackContract
{
	public const TYPE_IDEA = 'idea';

	public const TYPE_REPORT = 'report';
}
