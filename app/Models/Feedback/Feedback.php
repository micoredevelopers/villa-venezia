<?php

namespace App\Models\Feedback;

use App\Models\Model;

class Feedback extends Model
{
	protected $guarded = ['id'];

	protected $casts = [
		'data'  => 'array',
		'files' => 'array',
	];

	public function setFiles(array $files)
	{
		$this->setAttribute('files', $files);
	}
}
