<?php

namespace App\Models;

use App\Observers\UserObserver;
use App\Platform\Cabinet\Display\Customer\DisplayCustomerContainer;
use App\Platform\Cabinet\Display\Performer\DisplayPerformerContainer;
use App\Traits\EloquentExtend;
use App\Traits\EloquentScopes;
use App\Traits\Models\User\UserAccessorsTrait;
use App\Traits\Models\User\UserHelpersTrait;
use App\Traits\Models\User\UserMutatorTrait;
use App\Traits\Models\User\UserRelationTrait;
use App\Traits\Models\User\UserSettersTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
	use UserAccessorsTrait, UserMutatorTrait, UserHelpersTrait, UserRelationTrait, UserSettersTrait;

	use EloquentExtend, EloquentScopes;

	use Notifiable, HasFactory;

	private $type;

	private $customerContainer;
	private $performerContainer;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $guarded = [
		'id',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'profile_enabled'   => 'bool',
	];

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
	}

	public function setUserType($type): User
	{
		$this->type = $type;
		return $this;
	}

	public function getUserType(): ?string
	{
		return $this->type;
	}

	public static function boot()
	{
		parent::boot();

		if (class_exists(UserObserver::class)) {
			static::observe(UserObserver::class);
		}
	}

	public function getCustomerContainer()
	{
		if (is_null($this->customerContainer)) {
			$this->customerContainer = app(DisplayCustomerContainer::class, ['user' => $this]);
		}
		return $this->customerContainer;
	}

	public function getPerformerContainer()
	{
		if (is_null($this->performerContainer)) {
			$this->performerContainer = app(DisplayPerformerContainer::class, ['user' => $this]);
		}
		return $this->performerContainer;
	}

}
