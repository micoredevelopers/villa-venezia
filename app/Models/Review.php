<?php

namespace App\Models;

use App\Models\Model;

class Review extends Model
{
   protected $guarded = ['id'];

	public function getReview()
	{
		return $this->review;
   }
}
