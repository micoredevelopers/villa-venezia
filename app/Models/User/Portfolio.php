<?php

namespace App\Models\User;

use App\Models\Category\Category;
use App\Models\Model;
use App\Traits\Models\HasImages;
use App\Traits\Models\MainOrLangDescriptionAttributeTrait;
use App\Traits\Models\NameAttributeTrait;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Portfolio extends Model
{
	use HasImages;
	use NameAttributeTrait;
	use MainOrLangDescriptionAttributeTrait;

	protected $guarded = ['id'];

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function getCategory(): Category
	{
		return $this->category;
	}

}
