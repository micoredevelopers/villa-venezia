<?php

namespace App\Models\User;

use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserMeta extends Model
{
	protected $guarded = ['id'];

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}

	public function setKey($key): self
	{
		$this->setAttribute('key', $key);
		return $this;
	}

	public function setValue($value): self
	{
		$this->setAttribute('value', $value);
		return $this;
	}

	public function getValue()
	{
		return $this->getAttribute('value');
	}

}
