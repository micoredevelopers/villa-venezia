<?php

namespace App\Models\User;

use App\Models\Category\Category;
use App\Models\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Skill extends Model
{
	protected $guarded = ['id'];

	public function category(): BelongsTo
	{
		return $this->belongsTo(Category::class);
	}

	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}
}
