<?php

namespace App\Criteria\User;

use App\Containers\Admin\User\SearchDataContainer;
use App\Containers\Platform\User\SearchPerformerDataContainer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria;
 */
class PerformerFilterCriteria implements CriteriaInterface
{
	/**
	 * @var SearchPerformerDataContainer
	 */
	private $container;

	public function __construct(SearchPerformerDataContainer $container)
	{
		$this->container = $container;
	}

	/**
	 * Apply criteria in query repository
	 *
	 * @param string | Model $model
	 * @param RepositoryInterface $repository
	 *
	 * @return mixed
	 */
	public function apply($model, RepositoryInterface $repository)
	{
		if ($this->container) {
			if ($this->container->getCityId()) {
				$model = $model->where('city_id', $this->container->getCityId());
			}
			if ($this->container->getCategoryIds()) {
				$model = $model->whereHas('skills', function ($q) {
					$q->whereIn('category_id', $this->container->getCategoryIds());
				});
			}
			if ($search = $this->container->getSearch()) {
				$model = $model->where(function ($q) use ($search) {
					$q
						->whereLike('name', $search)
						->orWhereLike('surname', $search)
					;
				});
			}
		}
		return $model;
	}
}
