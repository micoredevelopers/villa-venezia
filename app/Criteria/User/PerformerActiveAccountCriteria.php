<?php

	namespace App\Criteria\User;

	use Illuminate\Database\Eloquent\Model;
	use Prettus\Repository\Contracts\CriteriaInterface;
	use Prettus\Repository\Contracts\RepositoryInterface;

	/**
	 * Class ActiveCriteria.
	 *
	 * @package namespace App\Criteria;
	 */
	class PerformerActiveAccountCriteria implements CriteriaInterface
	{

		/**
		 * Apply criteria in query repository
		 *
		 * @param string | Model      $model
		 * @param RepositoryInterface $repository
		 *
		 * @return mixed
		 */
		public function apply($model, RepositoryInterface $repository)
		{
			$model = $model->where('profile_enabled', 1);
			return $model;
		}
	}
