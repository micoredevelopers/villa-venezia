<?php

	namespace App\Mail;

	use Illuminate\Bus\Queueable;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;

	class MailAbstract extends Mailable implements ShouldQueue
	{

		public $tries = 3;

		use Queueable, SerializesModels;

		public function __construct($request = null)
		{
		}

		protected function getEmailFrom(): string
		{
			$default = 'info@' . $this->getHost();
			return getSetting('email.email-from', $default);
		}

		protected function getNameFrom(): string
		{
			return getSetting('email.name-from', $this->getHost());
		}

		protected function getEmailTo(): array
		{
			$emails = trim(getSetting('email.contact-email'), ' ');
			$emailTo = explode(',', $emails);
			/** @var  $emailsTo array*/
			$emailsTo = array_map('trim', $emailTo);
			return $emailsTo;
		}


		protected function getHost()
		{
			return parse_url(env('APP_URL'), PHP_URL_HOST);
		}

	}
