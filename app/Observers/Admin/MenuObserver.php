<?php

namespace App\Observers\Admin;

use App\Events\Admin\MenusChanged;
use App\Models\Menu;

class MenuObserver
{


	public function created()
	{
	}

	public function creating(Menu $menu)
	{
		$this->setUrlFromPage($menu);
	}

	public function updating(Menu $menu)
	{
		if (!$menu->originalIsEquivalent('menu_group_id', $menu->getAttribute('menu_group_id'))) {
			$menu->setAttribute('parent_id', 0);
		}
		$this->setUrlFromPage($menu);
	}

	public function updated()
	{
	}

	public function deleted()
	{
	}

	private function setUrlFromPage(Menu $menu)
	{
		if ($menu->page AND !$menu->url) {
			$menu->url = $menu->page->url;
		}
	}
}
