<?php

namespace App\Observers;

use App\Models\Verification;

class VerificationObserver
{
    /**
     * Handle the verification "created" event.
     *
     * @param  \App\Models\Verification  $verification
     * @return void
     */
    public function created(Verification $verification)
    {
        //
    }


	public function creating(Verification $verification)
	{
		$verification->setAttribute('available_at', now()->addMinute());
	}

    public function updating(Verification $verification)
    {
	    $verification->setAttribute('available_at', now()->addMinute());
    }



    public function updated(Verification $verification)
    {
        //
    }


    public function deleted(Verification $verification)
    {
        //
    }

    public function restored(Verification $verification)
    {
        //
    }

    public function forceDeleted(Verification $verification)
    {
        //
    }
}
