<?php


namespace App\Repositories\Admin;


use App\Models\Setting;
use App\Repositories\AbstractRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SettingsRepository extends AbstractRepository
{

	public function model()
	{
		return Setting::class;
	}

    public function getSettings(Request $request, $replace = false)
    {
        $query = Setting::with(['user']);

        if ($request->has('with_trashed')) {
            $query->withTrashed();
        }
        if ($search = $request->get('search')) {
            $query->where(function (Builder $query) use ($search) {
                $query->where('key', 'like', '%' . $search . '%')
                    ->orWhere('value', 'like', '%' . $search . '%')
                    ->orWhere('details', 'like', '%' . $search . '%')
                    ->orWhere('group', 'like', '%' . $search . '%')
                    ->orWhere('value', 'display_name', '%' . $search . '%');
            });
        }
        $settings = $query->get();
        if (!$replace && $settings->isEmpty()) {
            $request->merge(['search' => replaceLettersSearchRuEn($request->get('search'))]);
            return $this->getSettings($request, true);
        }
        if (!isSuperAdmin()) {
            $settings = $this->filterStaff($settings);
        }

        return $settings;
    }

    private function filterStaff(Collection $settings)
    {
        return $settings->filter(function (Setting $item) {
            $isStaff = Setting::isStaff($item->key);
            return !$isStaff;
        });
    }

}
