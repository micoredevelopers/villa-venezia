<?php

namespace App\Repositories\Admin;

use App\Models\Language;
use App\Repositories\AbstractRepository;
use Illuminate\Support\Collection;

/**
 * Class LanguageRepository.
 */
class LanguageRepository extends AbstractRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Language::class;
    }

    public function getForCreateEntity(): Collection
    {
        $languages = Language::active()->get();
        return $languages;
    }
}
