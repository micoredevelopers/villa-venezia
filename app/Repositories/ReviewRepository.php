<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\IsPublishedCriteria;
	use App\Models\Order\Order;
	use App\Models\Review;
	use App\Platform\Contract\UserTypeContract;
	use Illuminate\Contracts\Auth\Authenticatable;
	use Illuminate\Database\Eloquent\Builder;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Support\Collection;


	class ReviewRepository extends AbstractRepository
	{

		/**
		 * @return Review
		 * @throws \Prettus\Repository\Exceptions\RepositoryException
		 */
		public function makeModel()
		{
			return parent::makeModel();
		}


		public function model()
		{
			return Review::class;
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(ActiveCriteria::class)->setTable('reviews'));
//			$this->pushCriteria($this->app->make(IsPublishedCriteria::class)->setTable('reviews'));
			$this->applyCriteria()->resetCriteria();
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->paginate();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->get();
		}



	}

