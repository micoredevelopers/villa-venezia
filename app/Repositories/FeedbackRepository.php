<?php

namespace App\Repositories;

use App\Models\Feedback\Feedback;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class FeedbackRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class FeedbackRepository extends AbstractRepository
{
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model()
	{
		return Feedback::class;
	}

	public function getListAdmin()
	{
		return $this->all();
	}


}
