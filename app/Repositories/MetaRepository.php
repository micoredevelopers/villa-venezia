<?php

namespace App\Repositories;

use App\Models\Meta;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

//use Your Model

/**
 * Class MetaRepository.
 */
class MetaRepository extends AbstractRepository
{
	/**
	 * @return string
	 *  Return the model
	 */
	public function model(): string
	{
		return Meta::class;
	}

	public function getForAdminDisplay(Request $request): LengthAwarePaginator
	{
		/** @var  $query Builder */
		$query = Meta::with('lang');
		if ($search = $request->get('search')) {
			$query->join('meta_lang', $this->model->getForeignKey(), 'id');
			$query->where(static function (Builder $builder) use ($search) {
				$builder->where('url', 'like', '%' . $search . '%')
					->orWhere('title', 'like', '%' . $search . '%')
					->orWhere('description', 'like', '%' . $search . '%')
					->orWhere('keywords', 'like', '%' . $search . '%');
			});
			$query->orderBy('url');
		}
		$query->latest();
		return $query->paginate();
	}
}
