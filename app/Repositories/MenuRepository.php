<?php


namespace App\Repositories;


use App\Criteria\ActiveCriteria;
use App\Criteria\SortCriteria;
use App\Helpers\Debug\LoggerHelper;
use App\Models\Menu;
use App\Models\MenuGroup;
use App\Models\MenuLang;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class MenuRepository extends AbstractRepository
{
	/**
	 * @inheritDoc
	 */
	public function model()
	{
		return Menu::class;
	}

	public function modelLang()
	{
		return MenuLang::class;
	}


	protected function addPublicCriteriaToQuery()
	{
		$this->pushCriteria(ActiveCriteria::class);
		$this->pushCriteria(SortCriteria::class);
		return $this;
	}

	public function getMenus(): ?array
	{
		$this->addPublicCriteriaToQuery();
		$cacheKey = Menu::$publicMenusCacheKey . '.' . getCurrentLocale();
		if ((!$menus = \Cache::get($cacheKey))) {
			$menus = [];
			$groups = MenuGroup::all();
			foreach ($groups as $group) {
				try {
					$menus[$group->role] = $group->getGroupWithNestedMenu()->menus;
				} catch (\Exception $e) {
					app(LoggerHelper::class)->error($e);
					continue;
				}
			}
			try {
				\Cache::set($cacheKey, $menus);
			} catch (\Exception $e) {
				app(LoggerHelper::class)->error($e);
			}
		}

		return $menus;
	}

	public function findByGroup(string $group): Collection
	{
		return tap($this->addPublicCriteriaToQuery()->with('lang')
			->whereIn(app(MenuGroup::class)->getForeignKey(), function ($q) use ($group) {
				$q->select('id')->from(\DB::table(app(MenuGroup::class)->getTable())->where('role', $group));
			})
			->get(), function () {
			$this->resetCriteria()->resetModel();
		});
	}
}
