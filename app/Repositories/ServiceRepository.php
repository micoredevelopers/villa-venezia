<?php

	namespace App\Repositories;

	use App\Criteria\ActiveCriteria;
	use App\Criteria\SortCriteria;
	use App\Models\Order\Order;
	use App\Models\Service\Service;
	use App\Models\Service\ServiceLang;
	use App\Platform\Contract\UserTypeContract;


	class ServiceRepository extends AbstractRepository
	{

		/**
		 * @return Review
		 * @throws \Prettus\Repository\Exceptions\RepositoryException
		 */
		public function makeModel()
		{
			return parent::makeModel();
		}

		public function model()
		{
			return Service::class;
		}

		public function modelLang()
		{
			return ServiceLang::class;
		}

		public function addPublicCriteriaToQuery(): self
		{
			$this->pushCriteria($this->app->make(SortCriteria::class));
			$this->pushCriteria($this->app->make(ActiveCriteria::class));
			$this->applyCriteria()->resetCriteria();
			return $this;
		}

		public function addAdminCriteriaToQuery()
		{
		}

		public function getListAdmin()
		{
			$this->addAdminCriteriaToQuery();
			return $this->paginate();
		}

		public function getListPublic()
		{
			$this->addPublicCriteriaToQuery();
			return $this->get();
		}

	}

