<?php

return [
	'publish' => [
		'published_title' => 'Проект опубликован',
		'published_date'  => '',
		'published_views' => '{0}:views просмотров|{1}:views просмотр|[2,4]:views просмотра|[5,*]:views просмотров',
		'published_bids'  => '{0}:bids ставок|{1}:bids ставка|[2,4]:bids ставки|[5,*]:bids ставок',
		'now' => 'только что',
		'ago' => ':date назад'
	],
	'close'   => [
		'closed'    => 'Проект закрыт',
		'closes_at' => 'До закрытия',
	],
	'date' => [
		'glue' => ' и ',
	]
];
