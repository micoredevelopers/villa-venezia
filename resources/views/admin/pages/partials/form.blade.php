@include('admin.partials.crud.elements.title')

<div class="row">
    <div class="col-md-8">
        @include('admin.partials.crud.elements.url')
    </div>
    @if(isSuperAdmin())
        <div class="col-md-4">
            @include('admin.partials.crud.checkbox', ['name' => 'manual', 'title' => 'Manual url'])
        </div>
    @endif
</div>

@include('admin.partials.crud.elements.image-upload-group')

<div class="row">
    <div class="col-3">
        @include('admin.partials.crud.elements.active')
    </div>
</div>
<div class="row">
    @isset($pageTypes)
        <div class="col-4">
            {!! errorDisplay('page_type') !!}
            <label for="page_type">@lang('modules.pages.page_type')</label>
            {!! Form::select('page_type', ['' => __('form.select.empty')] + $pageTypes, $edit->page_type ?? null,
             ['class' => 'form-control selectpicker', 'data-live-search' => 'true']) !!}
        </div>
    @endisset
    <div class="col-4">
        @include('admin.partials.crud.select.parent-id', ['nullable' => true])
    </div>
</div>

@include('admin.partials.crud.textarea.description')

@include('admin.partials.crud.textarea.except')
