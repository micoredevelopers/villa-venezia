<?php /** @var $item \App\Models\Review */ ?>
<?php /** @var $permissionKey string */ ?>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
{{--            <th>{{ __('form.sorting') }}</th>--}}
            <th class="th-description">Имя</th>
            <th>Отображение</th>
            <th>Отзыв</th>
            <th class="text-right">
{{--                @can('create_' . $permissionKey)--}}
                    <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
{{--                @endcan--}}
            </th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="{{ $table ?? $list->isNotEmpty() ? $list->first()->getTable(): '' }}">
        @foreach($list as $item)
            <tr class="draggable" data-id="{{ $item->id }}" data-sort="{{ $item->sort }}">
{{--                <td>--}}
{{--                    @include('admin.partials.sort_handle')--}}
{{--                </td>--}}
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->name }}</a>
                </td>
                <td>
                    {{ translateYesNo((int)$item->getAttribute('active')) }}
                </td>
                <td>
                    {{ \Illuminate\Support\Str::limit($item->getReview(), 250) }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}