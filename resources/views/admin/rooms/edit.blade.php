<?php /** @var $edit \App\Models\Service\Service */ ?>

<form action="{{ route($routeKey.'.update', $edit->id) }}" method="post" class="form-horizontal"
      enctype="multipart/form-data">
    @csrf
    @method('patch')

    @include('admin.partials.submit_update_buttons')


    <ul class="nav nav-tabs mb-5" role="tablist">
        <li role="presentation">
            <a class="active" href="#dataDefault" aria-controls="home" role="tab"
               data-toggle="tab">@lang('modules._.tabs.main')</a>
        </li>
        <li role="presentation"><a href="#photosList" aria-controls="profile" role="tab"
                                   data-toggle="tab">@lang('modules._.tabs.photos')</a>
        </li>
    </ul>
    <section class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="dataDefault">
            <div class="panel-body">
                @include('admin.rooms.partials.form')
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="photosList">
            <div class="panel-body">
                @include('admin.additionalPhotos')
            </div>
        </div>

    </section>

    @include('admin.partials.submit_update_buttons')
</form>

