<?php /** @var $item \App\Models\Service\Service */ ?>
<?php /** @var $permissionKey string */ ?>
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th>{{ __('form.sorting') }}</th>
            <th class="th-description">@lang('form.title')</th>
            <th class="">@lang('form.image.image')</th>
            <th>Цена</th>
            <th>Отображение</th>
            <th class="text-right">
{{--                @can('create_' . $permissionKey)--}}
                    <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
{{--                @endcan--}}
            </th>
        </tr>
        </thead>
        <tbody data-sortable-container="true" data-table="{{ $table ?? $list->isNotEmpty() ? $list->first()->getTable(): '' }}">
        @foreach($list as $item)
            <tr class="draggable" data-id="{{ $item->id }}" data-sort="{{ $item->sort }}">
                <td>
                    @include('admin.partials.sort_handle')
                </td>
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->name }}</a>
                </td>
                <td>
                    <div class="img-container">
                        <a href="{{ imgPathOriginal(getPathToImage($item->image)) }}" class="fancy" data-fancybox="images-module">
                            <img src="{{ getPathToImage($item->image) }}" alt=""/>
                        </a>
                    </div>
                </td>
                <td>
                    {{ $item->price }} {{ getCurrencyIcon() }}
                </td>
                <td>
                    {{ translateYesNo((int)$item->getAttribute('active')) }}
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$list->render()}}