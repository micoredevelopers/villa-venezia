<div class="row">
	<div class="col-6">
		@if(isSuperAdmin())
		<a href="{{ route('menu-group.index') }}" class="btn btn-sm">GROUPS</a>
		@endif
	</div>
	<div class="col-6 text-right">
		<a href="{{ route($routeKey . '.create') }}?group={{ $group->id }}" class="btn btn-primary">@lang('form.create')</a>
	</div>
</div>

<?php /** @var $menu \App\Models\Menu */ ?>

@php
	$menus = $list;
@endphp
<div class="dd menu">
	@include('admin.menu.partials.menu-loop')
</div>


<script>
    $(document).ready(function () {
        const nestableUrl = '{{ route('admin.menu.nesting') }}';
        $('.dd').nestable({
            callback: function(l,e){
                $serialized = $('.dd').nestable('serialize');
                updateNesting($serialized);
            }
        });

        function updateNesting(data) {
            const $data = {
                'menus': data,
            };
            $.post(nestableUrl, $data,
				function (data) {
                if (data.message) {
                    let $status = (data.status) ? 'success' : 'error';
                    message(data.message, $status);
                }
            })
        }
    });
</script>
