@include('admin.partials.crud.elements.name')

<div class="row">
    <div class="col-6">
        @include('admin.partials.crud.elements.active')
    </div>
    <div class="col-6">
        @include('admin.partials.crud.checkbox', ['name' => 'free', 'title' => 'Бесплатно'])
    </div>
</div>

<div class="row">
    <div class="col-6">
        <label class=" control-label">Главное изображение</label>
        @include('admin.partials.crud.elements.image-upload-group')
    </div>
    <div class="col-6">
        <label class=" control-label">Значок</label>
        @include('admin.partials.crud.elements.image-upload-group', ['name' => 'icon'])
    </div>
</div>


@include('admin.partials.crud.textarea.except')

@include('admin.partials.crud.textarea.description')