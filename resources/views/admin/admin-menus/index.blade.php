<form action="{{route('admin-menus.updateAll')}}" method="post">
    <input type="hidden" name="_method" value="PATCH">
    @csrf
    <div class="panel panel-default">
        <div class="panel-body clearfix">
            <a href="{{route($routeKey.'.create')}}" class="btn btn-default">Добавить +</a>
            <button class="btn btn-primary">Сохранить</button>
            <a class="btn btn-warning" href="?seed=true">Seed</a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover table-condensed table-striped">
            <tr class="active">
                <th>ID</th>
                <th>@lang('form.sorting')</th>
                <th>@lang('form.title')</th>
                <th>Родитель</th>
                <th>Значок шрифт</th>
                <th>Включен</th>
                <th>URL</th>
                <th>Gate</th>
            </tr>
            @if(isset($list) AND $list->isNotEmpty())
                <tbody data-sortable-container="true" data-table="{{ $table ?? $list->first()->getTable() }}">
                @forelse($list as $item)
					<?php /** @var $item \App\Models\Admin\AdminMenu */ ?>
                    @php
                        $id = $item->getKey();
                        $inputManager = inputNamesManager($item);
                    @endphp
                    <tr class="draggable" data-id="{{ $id }}" data-sort="{{ $item->sort }}">
                        <td>
                            <a href="{{ route($routeKey . '.edit', $id) }}">{{$id}}</a>
                            <input type="hidden" name="{{ $inputManager->getNameInputByKey('id') }}" value="{{$id}}">
                        </td>
                        <td>
                            @include('admin.partials.sort_handle')
                        </td>
                        <td>
                            <input type="text" name="{{ $inputManager->getNameInputByKey('name') }}" value="{{$item->name}}" class="form-control">
                        </td>
                        <td>
                            <select name="{{ $inputManager->getNameInputByKey('parent_id') }}" class="form-control selectpicker" data-live-search="true" autocomplete="off">
                                <option value="0" {{ ($item->parent_id == 0) ? 'selected' : '' }}>-- Нет --</option>
                                @foreach($list as $listItem)
                                    @if ($listItem->id != $id)
                                        <option value="{{ $listItem->id }}" {{ ($item->parent_id == $listItem->id) ? 'selected' : '' }}>{{ $listItem->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="{{ $inputManager->getNameInputByKey('icon_font') }}" value="{{$item->icon_font}}">
                        </td>
                        <td>
                            <div class="check-styled">
                                <input type="checkbox" value="1" id="active-{{$id}}"
                                       name="{{ $inputManager->getNameInputByKey('active') }}" {!! checkedIfTrue((int)$item->active) !!}
                                       data-send-no-disable="true"/>
                                <label for="active-{{$id}}"></label>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="{{ $inputManager->getNameInputByKey('url') }}" value="{{$item->url}}" class="form-control">
                        </td>
                        <td>
                            <input type="text" name="{{ $inputManager->getNameInputByKey('gate_rule') }}" value="{{$item->gate_rule}}"
                                   class="form-control">
                        </td>
                    </tr>
                @empty
                    <h3>Пусто</h3>
                @endforelse
                </tbody>
            @endif
        </table>
    </div>
</form>
