@extends('public.layout.app')

<main id="not-fount-page">
    <section class="not-found-section">
        <div class="not-found-image">
            <img src="{{ asset('/images/NotFound.svg') }}" alt="404">
        </div>
        <a href="/" class="custom-btn">
            <span>На главную</span>
        </a>
    </section>
</main>

@push('js')
    <script>
        window.onload = function() {
            document.querySelectorAll('.footer')[0].remove();
        };
    </script>
@endpush