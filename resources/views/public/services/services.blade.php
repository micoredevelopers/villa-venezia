<?php /** @var $services \App\Models\Service\Service[] */ ?>

<main>
    @includeIf('public.layout.includes.search_form')
    <div class="container-fluid">
        <div class="intro__images wow fadeIn" data-wow-delay=".3s">
            <div class="intro__images_item">
                <img src="/images/icons/aboutUs/item1.jpeg"/>
            </div>
            <div class="intro__images_item">
                <img src="/images/icons/aboutUs/item2.jpeg"/>
            </div>
        </div>
    </div>
    <section class="services__header">
        <div class="container-fluid">
            <div class="services__header_box">
                <div class="services__header_text">
                    <h2
                            class="services__title title wow fadeIn"
                            data-wow-delay=".2s"
                    >
                        Услуги
                    </h2>
                    <p
                            class="services__subtitle subtitle wow fadeIn"
                            data-wow-delay=".4s"
                    >
                        Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа
                        Аркадия на юге Одессы. В числе удобств бесплатный Wi-Fi и
                        бесплатная охраняемая парковка. В отеле работают крытый и
                        открытый бассейны, а также имеется гидромассажная ванна под
                        открытым небом. Отель «Вилла Венеция» расположен в 5 минутах
                        ходьбы от пляжа Аркадия на юге Одессы. В числе удобств
                        бесплатный Wi-Fi и бесплатная охраняемая парковка. В отеле
                    </p>
                </div>
                <a
                        class="services_button btn-custom wow fadeIn"
                        href='{{ route('reservation') }}'
                        data-wow-delay=".3s"
                >
                    Забронировать
                </a>
            </div>
        </div>
    </section>
    @foreach($services as $service)

        <div class="about-us services__elements {{ $loop->even ? 'reverse' : '' }}">
            <div class="container-fluid">
                <div class="about-us__content">
                    <div class="about-us__content_img wow fadeInLeft">
                        <img src="{{ getPathToImage($service->getImage()) }}" alt="{{ $service->name }}"/>
                    </div>
                    <div class="about-us__content_text wow fadeIn" data-wow-delay=".2s">
                        @if($service->free)
                            <span class="suptitle">{{ getTranslate('services.free', 'Бесплатно') }}</span>
                        @endif
                        <h3 class="about-us__content_title title">{{ $service->name }}</h3>
                        <div class="about-us__content_description subtitle">
                            <div>
                                {!! nl2br($service->description) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach


    <section>
        <div class="containerForm">
            <div class="services__form formBlock">
                @includeIf('public.layout.elements.form-subtext')
            </div>
        </div>
    </section>
</main>


@includeIf('public.layout.includes.footer')