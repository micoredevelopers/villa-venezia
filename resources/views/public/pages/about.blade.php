<main>
    <section class="about-us">
    @includeIf('public.layout.includes.search_form')
        <div class="container-fluid">
            <div class="about-us__content">
                <div class="about-us__content_img wow fadeInLeft">
                    <img src="/images/icons/aboutUs/Villa.jpg" alt="about us photo"/>
                </div>
                <div class="about-us__content_text wow fadeIn" data-wow-delay=".2s">
                    <h1 class="about-us__content_title title">{{ showMeta(getTranslate('about.title', 'О нас')) }}</h1>
                    <div class="about-us__content_description subtitle">
                        <p>
                        {!! nl2br(getTranslate('contacts.top-description')) !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @includeIf('public.home.rooms')

    <section>
        <div class="containerForm">
            <div class="aboutUs__form formBlock">
               @includeIf('public.layout.elements.form-subtext')
            </div>
        </div>
    </section>
</main>

@includeIf('public.layout.includes.footer')