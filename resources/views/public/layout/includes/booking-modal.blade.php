<div class="popup" id="formPopup">
    <div class="popup__block">
        <div class="popup__block_close close-popup">
            <span></span><span></span>
        </div>
        <div class="popup__block_form formBlock">
            <form class="form" action="{{ route('reservation') }}">
                <div class="datepicker-input">
                    @includeIf('public.layout.elements.elements.arrival-input', ['inputName' => 'arrivalPopup',])
                </div>
                <div class="datepicker-input">
                    @includeIf('public.layout.elements.elements.departure-input', ['inputName' => 'departurePopup',])
                </div>
                <div class="guests-input">
                    @includeIf('public.layout.elements.elements.guests-input', ['inputName' => 'guestsPopup',])
                </div>
                <button class="btn-custom form__button">{{ getTranslate('form.search', 'Искать') }}</button>
            </form>
        </div>
    </div>
</div>


