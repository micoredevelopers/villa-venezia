<div class="footer__reservation wow fadeInLeft">
    <h3 class="footer__reservation_title titleMin">
{{ getTranslate('footer.title', 'Уютный мини-отель ') }}
    </h3>
    <p class="footer__subtitle">
        {{ getTranslate('footer.description', 'Отель расположен в курортной зоне Одессы — Аркадии. Улица Дерибасовская, Потёмкинская лестница, а также Музей Пушкина находятся в 6 км. ') }}
    </p>
    <a href='{{ route('reservation') }}' class="btn-custom footer__reservation_btn ">
        {{ getTranslate('footer.reservate', 'Забронировать') }}
    </a>
</div>
<div class="footer__contacts wow fadeInRight">
    <h3 class="footer__contacts_title titleMin">{{ getTranslate('footer.contacts', 'Контакты') }}</h3>
    <ul class="footer__contacts_items">
        <li class="footer__contacts_item">
            <a class="street" href="{{ getSetting('global.address') }}" target="_blank">{{ getTranslate('footer.contacts.address', 'Ул. Каманина,16') }}</a>
        </li>
        <li class="footer__contacts_item">
            <a class="tel" href="tel:+{{ extractDigits(getSetting('global.phone-one')) }}"
            >{{ getSetting('global.phone-one') }}</a>
        </li>
        <li class="footer__contacts_item">
            <a class="tel" href="tel:+{{ extractDigits(getSetting('global.phone-two')) }}">{{ getSetting('global.phone-two') }}</a>
        </li>
        <li class="footer__contacts_item">
            <a class="mail" href="mailto:{{ getSetting('global.public-email') }}">{{ getSetting('global.public-email') }}</a>
        </li>
        <li class="footer__contacts_item">
            @if(getSetting('socials.instagram'))

            <a class="inst" href="{{ getSetting('socials.instagram') }}" target="_blank">
                <img src="/images/icons/IconInstGold.svg"/>
            </a>
            @endif
            @if(getSetting('socials.facebook'))
                <a class="fb" href="{{ getSetting('socials.facebook') }}" target="_blank" rel="nofollow">
                    <img src="/images/icons/IconFbGold.svg"/>
                </a>
            @endif
        </li>
    </ul>
</div>