<footer class="footer">
    <div class="container-fluid">
        <div class="footer__map">
            @includeIf('public.layout.includes.footer-map')
        </div>
        <div class="footer__content">
            @includeIf('public.layout.includes.footer-content')
        </div>
        <div class="footer__copiryght wow flipInX" data-wow-delay=".2s">
            @includeIf('public.layout.includes.footer-copyright')
        </div>
    </div>

</footer>