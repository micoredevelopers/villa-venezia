<style>
  #block-search-main {
    background-color: unset;
    width: 100%;
    box-sizing: border-box;
  }
  .tl-container-main {
    padding: 0;
  }
</style>
<!-- start TL Search form script -->
<div id='block-search-main'>
    <div id='tl-search-form' class='tl-container-main'>
        <noindex><a href='http://travelline.ua/' rel='nofollow'>система онлайн-бронирования</a></noindex>
    </div>
</div>
<script type='text/javascript'>
    (function(w) {
        var q = [
            ['setContext', 'TL-INT-villa-venezia-com-ua.main', '{{ $viewLanguage->getCurrentLanguageCode() }}'],
            ['embed', 'search-form', {
                container: 'tl-search-form'
            }]
        ];
        var t = w.travelline = (w.travelline || {}),
            ti = t.integration = (t.integration || {});
        ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
        if (!ti.__loader) {
            ti.__loader = true;
            var d = w.document,
                p = d.location.protocol,
                s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
            (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
        }
    })(window);
</script>
<!-- end TL Search form script -->