<?php /** @var $languages \App\Containers\View\ViewLanguageInterface[] */ ?>
<?php /** @var $menus \App\Models\Menu[] */ ?>
<header class="header {{ $headerClass ?? '' }}" id="fixedHeader">
    <div class="container-fluid">
        <nav class="header__nav">
            <div class="header__links">
                @foreach($menus ?? [] as $menu)
                    <a class="header__links_item" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a>
                @endforeach
                <div class="header__multi">
                    @foreach($languages as $language)
                        <a class="header__multi_item {{ $language->getIsActive() ? 'active' : ''}}" href="{{ $language->getUrl() }}"
                           hreflang="{{ $language->getIsoKey() }}"
                           rel="alternate">{{ $language->getName() }}</a>
                    @endforeach
                </div>
                <select class="header__multi_select" id="{{ isMainPage() ? 'fixedMulti' : '' }}" onchange="location.href = this.value">
                    @foreach($languages as $language)
                        <option value="{{ $language->getUrl() }}" {{ $language->getIsActive() ? 'selected' : '' }}>{{ $language->getName() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="header__logo">
                <a href="{{ route('home') }}"><img src="{{ isMainPage() ?'/images/icons/logo.svg' : '/images/icons/logoDarkBig.svg' }}" alt="logo"
                                 id="{{ isMainPage() ? 'fixedImg' : '' }}"/></a>
            </div>
            <div class="header__contacts">
                <a class="header__contacts_map" href="{{ getSetting('global.address') }}" rel="nofollow" target="_blank">
                    <img src="{{ isMainPage() ? '/images/icons/IconMap.svg' : asset('images/icons/IconMapGold.svg') }}" alt="Map"/>
                    <span>{{ getTranslate('footer.contacts.address', 'Ул. Каманина,16') }}</span></a>
                <a class="header__contacts_tel" href="tel:+{{ extractDigits(getSetting('global.phone-one')) }}">
                    <img src="{{ isMainPage() ? '/images/icons/IconTel.svg' : '/images/icons/IconTelGold.svg' }}" alt="Phone" id="{{ isMainPage() ? 'fixedPhone' : '' }}"/><span>{{ getSetting('global.phone-one') }}</span>
                </a>
                <div class="burger" id="open">
                    <span class="{{ isMainPage() ? 'fixed' : '' }}"></span><span class="{{ isMainPage() ? 'fixed' : '' }}"></span>
                </div>
            </div>
        </nav>
        <div class="modal__container">
            <div class="header__nav">
                <div class="header__links">
                    <select class="header__multi_select" onchange="location.href = this.value">
                        @foreach($languages as $language)
                            <option value="{{ $language->getUrl() }}" {{ $language->getIsActive() ? 'selected' : '' }}>{{ $language->getName() }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="header__logo">
                    <a href="/">
                        <img src="{{ isMainPage() ?'/images/icons/logo.svg' : '/images/icons/logoDarkBig.svg' }}" alt="logo"/></a>
                </div>
                <div class="header__contacts">
                    <a class="header__contacts_tel" href="tel:+{{ extractDigits(getSetting('global.phone-one')) }}">
                        <img src="/images/icons/IconTelDark.svg" alt="Phone"/>
                    </a>
                    <div class="burger active" id="close">
                        <span></span><span></span>
                    </div>
                </div>
            </div>
            <div class="modal__links">
                @foreach($menus ?? [] as $menu)
                    <a class="modal__link title" href="{{ $menu->getFullUrl() }}">{{ $menu->getName() }}</a>
                @endforeach
                <div class="footer__contacts_item">
                    <a class="street modal__street" href="{{ getSetting('global.address') }}" target="_blank"
                       rel="nofollow">{{ getTranslate('footer.contacts.address', 'Ул. Каманина,16') }}</a>
                </div>
            </div>
        </div>
    </div>
</header>