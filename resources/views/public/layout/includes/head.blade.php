<meta charset="utf-8"/>
{!! SEOMeta::generate() !!}
{!! getSettingIfProd('google.gtag-head') !!}
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="ie=edge, chrome=1"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="shortcut icon" href="/images/icons/favicon.png" type="image/png"/>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css"/>
<link rel="preconnect" href="https://fonts.gstatic.com"/>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&amp;display=swap" rel="stylesheet"/>
<link href="https://db.onlinewebfonts.com/c/a274c9556f742d6d7af83e629fc1e34f?family=Anglecia+Pro+Dsp+Md" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" type="text/css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" type="text/css"/>

<!-- favicon -->
{{--<link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>--}}

<link rel="stylesheet" type="text/css"  href="{{ mix('static/css/style.css') }}"/>

@yield('css')
@stack('css')

@yield('js-head')
@stack('js-head')

<!-- start TL head script -->
<script type='text/javascript'>
    (function(w) {
        var q = [
            ['setContext', 'TL-INT-villa-venezia-com-ua', 'ru']
        ];
        var t = w.travelline = (w.travelline || {}),
            ti = t.integration = (t.integration || {});
        ti.__cq = ti.__cq? ti.__cq.concat(q) : q;
        if (!ti.__loader) {
            ti.__loader = true;
            var d = w.document,
                p = d.location.protocol,
                s = d.createElement('script');
s.type = 'text/javascript';
            s.async = true;
            s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
            (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
        }
    })(window);
</script>
<!-- end TL head script -->
