<div class="formBlock__text wow fadeInLeft">
    <h3 class="formBlock__title title">{{ getTranslate('form.middle-title', 'Мини-отель в живописном уголке Одессы') }}</h3>
    <div class="formBlock__subtitle subtitle">{!! getTranslate('form.middle-description') !!}</div>
</div>