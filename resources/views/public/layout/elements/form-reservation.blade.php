<form class="form {{ $animated ?? true ? 'wow fadeInRight' : '' }}" action="{{ route('reservation') }}">
    @includeIf('public.layout.includes.search_form_main')
</form>
