<label for="{{ $inputName ?? 'arrival' }}"
>{{ getTranslate('form.in', 'Заезд') }}<img src="{{ asset('/images/icons/calendar.svg') }}"/></label
><input type="text" id="{{ $inputName ?? 'arrival' }}" name="date"/>