<label for="{{ $inputName ?? 'departure' }}"
>{{ getTranslate('form.out', 'Выезд') }}<img src="{{ asset('/images/icons/calendar.svg') }}"/></label
><input type="text" id="{{ $inputName ?? 'departure' }}" name="departure"/>