<label for="guests">{{ getTranslate('form.guests', 'Количество гостей') }}
    <img src="{{ asset('/images/icons/Guests.svg') }}"/>
</label>
<div class="guests-placeholder">
    <input
            type="number"
            placeholder="число"
            id="guests"
            value="1"
            min="1"
            name="{{ $inputName ?? 'guests' }}"
            inputmode="numeric"
            onkeyup="this.value = this.value.replace(/[A-Za-zА-Яа-яЁё]/,'');"
    /><!--span.guests-label гость-->
</div>