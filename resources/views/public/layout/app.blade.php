<!DOCTYPE html>
<html lang="{{ getCurrentLocale() }}">

<head>
    @include('public.layout.includes.head')
</head>
<body>
{!! getSettingIfProd('google.gtag-body') !!}

@include('public.layout.includes.header')


<div class="wrapper">
    @hasSection('content')
        @yield('content')
    @else
        {!! $content ?? '' !!}
    @endif
</div>



@include('public.layout.includes.booking-modal')

<x-adminSidebar></x-adminSidebar>

@include('public.layout.includes.assets.scripts')

</body>
</html>
