<div class="popup" id="reviewsPopup">
    <div class="popup__block">
        <div class="popup__block_close close-popup">
            <span></span><span></span>
        </div>
        <form class="popup__block_form" action="{{ route('reviews.store') }}" method="post">
            @csrf
            <h3 class="popup__block_title title">{{ getTranslate('main.reviews.add', 'Оставить отзыв') }}</h3>
            <div class="popup__block_subtitle subtitle">
                <p>{{ getTranslate('reviews.modal-description', 'Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. ') }}</p>
            </div>
            <div class="popup__block_input">
                <label for="reviewsName">{{ getTranslate('form.full_name', 'Фамилия, имя') }}</label>
                <input name="name" type="text" id="reviewsName" placeholder="{{ getTranslate('form.full_name-placeholder', 'Андреев Андрей') }}"/>
            </div>
            <div class="popup__block_input">
                <label for="review">{{ getTranslate('form.review', 'Отзыв') }}</label>
                <textarea id="review" name="review" placeholder="{{ getTranslate('form.review-placeholder', 'Расскажите о нас') }}" ></textarea>
            </div>
            <button class="popup__block_btn btn-custom">{{ getTranslate('form.send', 'ОТПРАВИТЬ') }}</button>
        </form>
    </div>
</div>