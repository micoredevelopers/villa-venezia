<?php /** @var $loop \App\Helpers\Dev\BladeLoopAutocomplete */ ?>
<?php /** @var $services \App\Models\Service\Service[] */ ?>


<section class="services">
    <div class="container-fluid">
        <h3 class="services__title title wow fadeInUp">{{ getTranslate('main.services.title', 'Сервис и развлечения') }}</h3>
        <div class="services__items">
            @foreach($services as $service)
                <div class="services__item wow fadeIn" data-wow-delay=".{{ $loop->iteration }}s">
                    <div class="services__item_img">
                        <img src="{{ getPathToImage($service->icon) }}" alt="{{ $service->name }}"/>
                    </div>
                    <div class="services__item_title">
                        <h3>{{ $service->name }}</h3>
                    </div>
                    <div class="services__item_subtitle">
                        <div>{!! $service->excerpt !!}</div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="services__slick wow fadeInLeft">
            @foreach($services as $service)
                <div class="services__item wow fadeIn">
                    <div class="services__item_img">
                        <img src="{{ getPathToImage($service->icon) }}" alt="{{ $service->name }}"/>
                    </div>
                    <div class="services__item_title">
                        <h3>{{ $service->name }}</h3>
                    </div>
                    <div class="services__item_subtitle">
                        <div>{!! $service->excerpt !!}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>