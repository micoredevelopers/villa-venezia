<?php /** @var $reviews \App\Models\Review[] */ ?>
@isset($reviews)
    <section class="reviews">
        <div class="container-fluid">
            <div class="reviews__header wow fadeInUp">
                <h3 class="reviews__title title">{{ getTranslate('main.reviews.title', 'Отзывы') }}</h3>
                <button class="reviews_btn btn-custom open-popup">{{ getTranslate('main.reviews.add', 'Оставить отзыв') }}</button>
            </div>
            <div class="reviews__slider">
                @foreach($reviews as $review)
                    <div class="reviews__slider_item wow fadeInUp" data-wow-delay=".{{ $loop->iteration }}s">
                        <h4 class="reviews__slider_name">{{ $review->name }}</h4>
                        <div class="reviews__slider_description">{{ $review->getReview() }}</div>
                        @if(\Illuminate\Support\Str::length( $review->getReview()) > 220)
                            <button class="moreBtn">
                                <span class="more_text">{{ getTranslate('reviews.show-full', 'Показать польностью') }}</span>
                                <span class="less_text">{{ getTranslate('reviews.show-less', 'Скрыть') }}</span>
                            </button>
                        @endif
                    </div>
                @endforeach
            </div>
            <button class="reviews_btn btn-custom bottom open-popup">{{ getTranslate('main.reviews.add', 'Оставить отзыв') }}</button>
        </div>
    </section>
@endisset