@includeWhen(false, 'public.layout.app')

<main>
    <section class="intro">
        <div class="containerForm">
            <div class="intro__formBlock formBlock">
                <div class="formBlock__text wow fadeInLeft">
                    <h1 class="formBlock__title title">{{ showMeta(getTranslate('form.middle-title', 'Мини-отель в живописном уголке Одессы')) }}</h1>
                    <p class="formBlock__subtitle subtitle">
                        {{ getTranslate('form.middle-description') }}
                    </p>
                </div>
                @includeIf('public.layout.elements.form-reservation')
            </div>
        </div>
    </section>
    <section class="feelSun wow fadeIn">
        <div class="container-fluid">
            <div class="feelSun__content">
                <div class="feelSun__content_header wow fadeInUp">
                    <h2 class="feelSun__content_title title">
                        {{ getTranslate('main.about.title', 'Почувствуйте тепло Одесского солнца в Вилле') }}
                    </h2>
                    <div class="feelSun__content_subtitle">
                        <p class="subtitle">
                            {{ getTranslate('main.about.description') }}
                        </p>
                    </div>
                </div>
                <div class="feelSun__content_firstImg">
                    <img src="/images/icons/feelSun/item1.jpg"/>
                    <div class="feelSun__content_secondImg wow fadeInRight">
                        <img src="/images/icons/feelSun/item2.jpg"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @includeIf('public.home.services')

    @includeIf('public.home.rooms',  ['watchAll' => true])

    <section class="infrastructure">
        <div class="container-fluid">
            <div class="infrastructure__content">
                <h3 class="infrastructure__header_title title wow fadeIn"
                    data-wow-delay=".1s">{{ getTranslate('main.infrastructure.title', 'Инфраструктура') }}</h3>
                <div class="infrastructure__header_subtitle wow fadeIn" data-wow-delay=".2s">
                    <p class="subtitle">
                        {{ getTranslate('main.infrastructure.description', 'Отель “Вилла Венеция” находится в известнейшем курортном районе — Аркадия, всего в пяти минутах ходьбы от пляжа. В непосредственной близости лучшие рестораны и ночные клубы города. ') }}
                    </p>
                </div>
                <div class="infrastructure__content_img wow fadeIn" data-wow-delay=".3s">
                    <img src="/images/icons/infastructure/img1.jpg" alt="pool"/>
                </div>
                <div
                        class="infrastructure__content_text wow fadeIn"
                        data-wow-delay=".4s"
                >
                    <div class="infrastructure__content_text-item">
                        <div class="infrastructure__item_header">
                            <h4 class="infrastructure__item_title titleMin">{{ getTranslate('main.infrastructure.one.num', '5 минут') }}</h4>
                            <div class="infrastructure__item_subtitle">
                                <p>{{ getTranslate('main.infrastructure.one.target', 'до пляжа и ночных клубов') }}</p>
                            </div>
                        </div>
                        <div class="infrastructure__item_content">
                            <p class="subtitle">
                                {{ getTranslate('main.infrastructure.one.description', 'Всего 5 минут пешей ходьбы к морю. Рядом любимый пляж Одессы — “Аркадия”, а также оживленные ночные клубы, в том числе знаменитые «Ибица» и «Рай». ') }}
                            </p>
                        </div>
                    </div>
                    <div class="infrastructure__content_text-item">
                        <div class="infrastructure__item_header">
                            <h4 class="infrastructure__item_title titleMin">{{ getTranslate('main.infrastructure.two.num', '10 минут ') }}</h4>
                            <div class="infrastructure__item_subtitle">
                                <p>{{ getTranslate('main.infrastructure.two.target', 'до ж/д и автовокзала') }}</p>
                            </div>
                        </div>
                        <div class="infrastructure__item_content">
                            <p class="subtitle">{{ getTranslate('main.infrastructure.two.description', 'Железнодорожный вокзал и автовокзал расположены всего в 10-15 минутах езды на автомобиле. ') }}</p>
                        </div>
                    </div>
                    <div class="infrastructure__content_text-item">
                        <div class="infrastructure__item_header">
                            <h4 class="infrastructure__item_title titleMin">
                                {{ getTranslate('main.infrastructure.three.num', '15 минут ') }}
                            </h4>
                            <div class="infrastructure__item_subtitle">
                                <p>{{ getTranslate('main.infrastructure.three.target', 'до центра города') }}</p>
                            </div>
                        </div>
                        <div class="infrastructure__item_content">
                            <p class="subtitle">{{ getTranslate('main.infrastructure.three.description', 'Отель расположен в курортной зоне Одессы — Аркадии. Улица Дерибасовская, Потёмкинская лестница, а также Музей Пушкина находятся в 6 км. ') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @includeIf('public.home.reviews')
    @includeIf('public.home.reviews-modal')

</main>

@if (session('successReview'))
    <div class="popup" id="reviewsSuccessPopup">
        <div class="popup__block">
            <div class="popup__block_close close-popup">
                <span></span><span></span>
            </div>
            <div class="popup__block_form">
              <h3 class="popup__block_title title">Спасибо за отзыв</h3>
                <div class="popup__block_subtitle subtitle">
                  <p>{{ getTranslate('reviews.modal-description', 'Отель «Вилла Венеция» расположен в 5 минутах ходьбы от пляжа Аркадия на юге Одессы. ') }}</p>
                </div>
                <button class="popup__block_btn btn-custom closeBtn" id='closeBtn'>ОКЕЙ</button>
            </div>
        </div>
    </div>

    <script>
        window.onload = function (){
            $('#reviewsSuccessPopup').fadeIn(600);
            $('#reviewsSuccessPopup').click(function (e){
                      if(e.target.className === 'popup' || e.target.className === 'popup__block_close close-popup' || e.target.id === 'closeBtn') {
                          $('#reviewsSuccessPopup').fadeOut(600);
            }})
        }

    </script>
@endif

@include('public.layout.includes.footer')

