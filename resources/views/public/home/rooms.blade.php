<?php /** @var $loop \App\Helpers\Dev\BladeLoopAutocomplete */ ?>
<?php /** @var $rooms \App\Models\Room\Room[] */ ?>

@if(isset($rooms))

    <section class="rooms">
        <div class="container-fluid">
            <div class="rooms__header wow fadeInUp">
                <h3 class="rooms__header_title title">{{ getTranslate('main.rooms.title', 'Номера') }}</h3>
                @if($watchAll ?? false)
                <div class="rooms__header_link">
                        <a href="{{ url('/about') }}">{{ getTranslate('main.rooms.all', 'Смотреть все') }}</a>
                </div>
                @endif
            </div>
            <div class="rooms__slider">
                <div class="rooms__slider_items">
                    @foreach($rooms as $room)
                        <div class="rooms__slider_item wow fadeIn" data-wow-delay=".{{ $loop->iteration }}s">
                            <div class="rooms__slider_img">
                                <a href="{{ getPathToImage($room->getImage()) }}" data-caption="{{ $room->name }}"
                                   data-fancybox="rooms-{{ $room->getKey() }}">
                                    <img src="{{ getPathToImage($room->getImage()) }}" alt="{{ $room->name }}"/>
                                </a>
                                @foreach($room->getImages() as $image)
                                    <a href="{{ getPathToImage($image->getImage()) }}" data-caption="{{ $room->name }}"
                                       data-fancybox="rooms-{{ $room->getKey() }}">
                                        <img src="{{ getPathToImage($image->getImage()) }}" alt="{{ $room->name }}, {{ $loop->iteration }}"/>
                                    </a>
                                @endforeach
                            </div>
                            <h3 class="rooms__slider_title">{{ $room->name }}</h3>
                            <div class="rooms__slider_subtitle">{{ $room->excerpt }}</div>
                            <span class="rooms__slider_price"> {{ $room->price }} {{ getCurrencyIcon() }}</span>
                        </div>
                    @endforeach
                </div>
            </div>
            @if($watchAll ?? false)
                <div class="lastOrder"><a href="{{ url('/about') }}">{{ getTranslate('main.rooms.all', 'Смотреть все') }}</a></div>
            @endif
        </div>
    </section>
@endif