<?php /** @var $meta \App\Models\Meta */ ?>

<div class="admin-sidebar">
    <div class="admin-sidebar-container">
        <div class="button-close">
            <button class="btn"><i class="fa fa-times"></i></button>
        </div>
        {{--            --}}
        <div class="card">
            <div class="card-header">SEO</div>
            <div class="card-body">
                @isset($meta)
                    {!! linkEditIfAdmin($meta, 'Edit meta','default') !!}
                @else
                    <a class="btn btn-primary text-uppercase"
                       href="{{ route('admin.meta.create') }}?url={{ $metaUrlCreate ?? '' }}" target="_blank"><i
                                class="fa fa-plus"></i> Create meta</a>
                @endisset
                <a href="{{ asset('robots.txt') }}" target="_blank" class="btn {{ $robotsClass ?? '' }}">Robots.txt
                    <i class="fa fa-external-link" aria-hidden="true"></i></a>
                <a href="{{ asset('sitemap.xml') }}" target="_blank" class="btn {{ $sitemapClass ?? '' }}">Sitemap
                    <i class="fa fa-external-link" aria-hidden="true"></i></a>
            </div>
        </div>
        {{--                --}}
        @if($settingsSearch ?? false)
            <div class="card mt-3">
                <div class="card-header">Settings</div>
                <div class="card-body">
                    <form action="{{ route('settings.index') }}" method="get" target="_blank" class="form-inline">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                    @if($usedSettings ?? false)
                        Использованные настройки на данной странице:
                        @foreach($usedSettings as $usedSetting)
                            <a class="badge badge-secondary no-radius" target="_blank"
                               href="{{ route('settings.index') }}?search={{ $usedSetting }}">{{ displayUsedSetting($usedSetting) }}</a>
                        @endforeach
                    @endif
                </div>
            </div>
        @endif
        {{--            --}}
        <div class="card mt-3">
            <div class="card-header">Translate</div>
            <div class="card-body">
                <form action="{{ route('translate.index') }}" method="get" target="_blank" class="form-inline">
                    <div class="form-group">
                        <input type="text" name="search" class="form-control">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </form>
                @if($usedTranslates ?? false)
                    Использованные переводы на данной странице
                    @foreach($usedTranslates as $usedTranslate)
                        <a class="badge badge-secondary no-radius" target="_blank" title="{{ getTranslate($usedTranslate) }}"
                           href="{{ route('translate.index') }}?search={{ $usedTranslate }}">{{ \Illuminate\Support\Str::limit(getTranslate($usedTranslate), 50) }}</a>
                    @endforeach
                @endif
            </div>
        </div>
        @if($usersSearch ?? false)
            <div class="card mt-3">
                <div class="card-header">Users</div>
                <div class="card-body">
                    <form action="{{ route('admin.users.index') }}" method="get" target="_blank" class="form-inline">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        @endif
        @if($productionLink ?? false)
            <a class="btn btn-danger mt-2" href="{{ $productionLink }}" target="_blank">Production <i
                        class="fa fa-external-link"></i></a>
        @endif
        @if($canViewLogs ?? false)
            <a class="btn btn-secondary mt-2" href="{{ url('admin/logs') }}" target="_blank">Logs <i
                        class="fa fa-external-link"></i></a>
        @endif
        <div class="sidebar_rendered">
            @foreach($rendered ?? [] as $render)
                <div class="rendered_item">
                    {!! $render !!}
                </div>
            @endforeach
        </div>
        {{--            --}}
    </div>
    <div class="admin-sidebar-overlay"></div>
</div>


<style type="text/css">
    .admin-sidebar-overlay,
    .admin-sidebar-container {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
    }

    .admin-sidebar {
        z-index: 100000;
        position: fixed;
        left: -448px;
        top: 0;
        bottom: 0;
        width: 450px;
        transition: all ease .2s;
    }

    .admin-sidebar.active {
        left: 0;
    }

    .admin-sidebar-overlay {
        background: linear-gradient(to right, #83a4d4b3, #9bf4f9e0);
        border-right: 2px solid brown;
        z-index: 9;
        right: 0;
    }

    .admin-sidebar-container .button-close {
        position: absolute;
        right: 0;
        text-align: center;
        z-index: 100000;
        cursor: pointer;
    }

    .admin-sidebar-container {
        width: 446px;
        margin-left: 2px;
        z-index: 100000;
        overflow-y: auto;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

@push('js')
    <script>
        $(document).ready(function () {
            $('.admin-sidebar-overlay, .admin-sidebar-container .button-close').on('click', function () {
                $(this).parents('.admin-sidebar').toggleClass('active')
            })
        });
    </script>
@endpush