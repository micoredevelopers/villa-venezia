<?php /** @var $viewLanguage \App\Containers\View\ReservationLanguageViewDataInterface*/ ?>
<script type="text/javascript">
    (function(w){
        var langs = "ru";
        if (window.location.pathname.indexOf("/en/") !== "-1") {langs = "en";}
        var q=[
            ['setContext', 'TL-INT-villa-venezia-odessa', langs]
        ];
        var t=w.travelline=(w.travelline||{}),ti=t.integration=(t.integration||{});ti.__cq=ti.__cq?ti.__cq.concat(q):q;
        if (!ti.__loader){ti.__loader=true;var d=w.document,p=d.location.protocol,s=d.createElement('script');s.type='text/javascript';s.async=true;s.src=(p=='https:'?p:'http:')+'//eu-ibe.tlintegration.com/integration/loader.js';(d.getElementsByTagName('head')[0]||d.getElementsByTagName('body')[0]).appendChild(s);}
    })(window);

</script>

	<style>
    #tl-booking-form {
      margin:  100px auto;
      max-width: 1440px;
      width: 100%;
    }
  </style>
  <!-- start TL Booking form script -->
  <div id='tl-booking-form'>&nbsp;</div>
  <script type='text/javascript'>
      (function(w) {
          var q = [
              ['setContext', 'TL-INT-villa-venezia-com-ua', '{{ $viewLanguage->getCurrentLanguageCode() }}'],
              ['embed', 'booking-form', {
                  container: 'tl-booking-form'
              }]
          ];
          var t = w.travelline = (w.travelline || {}),
              ti = t.integration = (t.integration || {});
          ti.__cq = ti.__cq? ti.__cq.concat(q) : q;
          if (!ti.__loader) {
              ti.__loader = true;
              var d = w.document,
                  p = d.location.protocol,
                  s = d.createElement('script');
              s.type = 'text/javascript';
              s.async = true;
              s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
              (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
          }
      })(window);
  </script>
  <!-- end TL Booking form script -->

